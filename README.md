# calaAdmin

#### 介绍
我的第一个react + antd 开源项目，包含：自定义工作台、高性能表格、拖拽布局、标签页等。
个性主题切换，根据个人喜好切换。
新增自定义工作台；
新增流程设计；

#### 软件架构
软件架构说明 react + antdesign

#### 演示地址

[访问Demo](http://admin.caladog.com)

#### 安装教程

1.  安装依赖： yarn
2.  启动项目： yarn start
3.  打包项目： yarn build

#### 使用说明

![输入图片说明](https://foruda.gitee.com/images/1681224742223480239/22911b97_5644086.png "01.png")
![输入图片说明](https://foruda.gitee.com/images/1681224762382857540/81262fee_5644086.png "02.png")
![输入图片说明](https://foruda.gitee.com/images/1681224776472071844/5aa73cfd_5644086.png "03.png")
![输入图片说明](https://foruda.gitee.com/images/1681224794029356773/88e9cc67_5644086.png "04.png")


#### 项目应用

[访问Demo](http://test.caladog.com)  账号：test 密码：123456

#### 更新日志

【2023-05-08】 升级为 antdesign V5.4 版本

【2023-08-08】 升级为 antdesign V5.8 版本


#### 最后的话

创作不易，如果您喜欢本项目，请麻烦点亮左下的小星星。