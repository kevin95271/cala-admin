import { defineConfig } from 'umi';
import router from './router';

export default defineConfig({
  mfsu: {},
  dynamicImport: {
    loading: '@/components/PageLoading',
  },
  routes: router,
  ignoreMomentLocale: true,
  locale: {
    default: 'zh-CN',
    antd: false,
    baseNavigator: false,
  },
  // workerLoader:{}
})