const proSettings = {
  "navTheme": "light",
  "layout": "mix",
  "contentWidth": "Fluid",
  "fixedHeader": true,
  "fixSiderbar": true,
  "pwa": false,
  "headerHeight": 40,
  "siderWidth": 200,
  "isTabs": true,
  "colorPrimary": "#13C2C2",
  "splitMenus": false,
  "theme": "summerTheme"
}
export default proSettings;
