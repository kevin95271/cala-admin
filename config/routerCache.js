import router from './router';
import * as R from 'ramda';

const routeCache = {};
R.forEach((v) => {
  if (v.path) {
    routeCache[v.path] = v.name;
  }
  if (v.routes) {
    R.forEach((inner) => {
      if (inner.path) {
        routeCache[inner.path] = inner.name;
      }
      if (inner.routes) {
        R.forEach((deepInner) => {
          if (deepInner.path) {
            routeCache[deepInner.path] = deepInner.name;
          }
        }, inner.routes);
      }
    }, v.routes);
  }
}, router[2].routes);

export default routeCache;
