export default [
    {
        path: '/user',
        routes: [
            {
                path: '/user',
                component: '../layouts/UserLayout',
                routes: [
                    {
                        name: '登录',
                        path: '/user/login',
                        component: './user/login',
                    },
                ],
            },
        ],
    },
    {// newWindow
        path: '/new',
        routes: [
            {
                path: '/new',
                component: '../layouts/NewWindow',
                routes: [
                    {
                        name: '新窗口',
                        path: '/new/table/add/:userId/:title',
                        component: './table/table/add',
                    },
                ],
            },
        ],
    },
    {
        path: '/',
        component: '../layouts/MainLayout',
        routes: [
            {
                name: '工作台',
                path: '/dashboard',
                component: './dashboard',
            },
            {
                name: '表单',
                path: '/form',
                routes: [
                    {
                        name: '复杂表单',
                        path: '/form/multi',
                        component: './form/multi',
                    },
                    {
                        name: '简单表单',
                        path: '/form/simple',
                        component: './form/simple',
                    },
                ]
            },
            {
                name: '表格',
                path: '/table',
                routes: [
                    {
                        name: '普通表格',
                        path: '/table/table',
                        component: './table/table',
                    },
                    {
                        name: '新建表单',
                        path: '/table/table/add',
                        component: './table/table/add',
                    },
                    {
                        name: '高级表格',
                        path: '/table/grid',
                        component: './table/grid',
                    },                   
                ]
            },
            {
                name: '电子签章',
                path: '/signature',
                routes: [
                    {
                        name: '电子签章',
                        path: '/signature/seal',
                        component: './signature/seal',
                    }
                ]
            },
            {
                name: '拖动布局',
                path: '/layout',
                routes: [
                    {
                        name: '水平布局',
                        path: '/layout/horizontal',
                        component: './layout/horizontal'
                    },
                    {
                        name: '垂直布局',
                        path: '/layout/vertical',
                        component: './layout/vertical',
                    },
                    {
                        name: '混合布局',
                        path: '/layout/mix',
                        component: './/layout/mix'
                    }
                ]
            },
            {
                name: '流程管理',
                path: '/flow',
                routes: [
                    {
                        name: '流程设计',
                        path: '/flow/design',
                        component: './flow/design',
                    }
                ]
            },
            {
                name: '系统管理',
                path: '/system',
                routes: [
                    {
                        name: '用户管理',
                        path: '/system/users',
                        component: './system/users'
                    },
                    {
                        name: '角色管理',
                        path: '/system/roles',
                        component: './system/roles'
                    },
                    {
                        name: '权限管理',
                        path: '/system/rights',
                        component: './system/rights',
                    },
                    {
                        name: '部门管理',
                        path: '/system/departments',
                        component: './system/departments',
                    },
                ]
            },
        ]
    },

]
