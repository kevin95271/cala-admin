import React, { useState } from 'react';
import { setLocale } from 'umi';
import { Drawer, Form, Row, Col, Switch, Radio, Segmented } from 'antd';
import { createFromIconfontCN } from '@ant-design/icons';
import * as R from 'ramda';

const MyIcon = createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/c/font_4138236_ruatnx8t8dg.js'
});

export default (props) => {
  const [settingForm] = Form.useForm();

  const changeSettings = (v) => {
    let key = Object.keys(v)[0];
    let value = v[key];
    let n = R.assoc(key, value)(props.settings);
    if (key == "locale") {
      setLocale(value, false);
      localStorage.setItem("umi-locale", value);
    }
    props.onSettingChange(n);
    localStorage.setItem("settings", JSON.stringify(n));
  }

  const closeDrawer = () => {
    props.closeDrawer();
  }


  return (
    <Drawer
      title="系统设置"
      placement="right"
      width={320}
      closable={false}
      onClose={closeDrawer}
      open={props.visible}
    >
      <Form
        form={settingForm}
        layout="horizontal"
        initialValues={props.settings}
        onValuesChange={changeSettings}
      >
        <Row>
          <Col span={24}>
            <Form.Item label="模式" name="navTheme" labelCol={{ span: 6 }} size="small">
              <Segmented
                options={[
                  {
                    label: '白昼',
                    value: 'light',
                    icon: <MyIcon type="caladog-day"/>,
                  },
                  {
                    label: '暗夜',
                    value: 'realDark',
                    icon: <MyIcon type="caladog-night"/>,
                  },
                ]}
              />
            </Form.Item>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <Form.Item label="标签页" name="isTabs" labelCol={{ span: 6 }} valuePropName="checked">
              <Switch checkedChildren="是" unCheckedChildren="否" />
            </Form.Item>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <Form.Item label="菜单布局" name="layout" labelCol={{ span: 6 }}>
              <Radio.Group buttonStyle="solid" size="small">
                <Radio.Button value="side">侧边</Radio.Button>
                <Radio.Button value="top">顶部</Radio.Button>
                <Radio.Button value="mix">混合</Radio.Button>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <Form.Item label="主题" name="theme" labelCol={{ span: 6 }} size="small">
              <Radio.Group buttonStyle="solid" size="small">
                <Radio.Button value="">默认</Radio.Button>
                <Radio.Button value="springTheme">春</Radio.Button>
                <Radio.Button value="summerTheme">夏</Radio.Button>
                <Radio.Button value="autumnTheme">秋</Radio.Button>
                <Radio.Button value="winterTheme">冬</Radio.Button>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>

        <Row>
          <Col span={24}>
            <Form.Item label="颜色" name="colorPrimary" labelCol={{ span: 6 }} size="small">
              <Radio.Group buttonStyle="solid" size="small">
                <Radio.Button value="#F5222D">红色</Radio.Button>
                <Radio.Button value="#2F54EB">蓝色</Radio.Button>
                <Radio.Button value="#13C2C2">绿色</Radio.Button>
                <Radio.Button value="#722ED1">紫色</Radio.Button>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>

      </Form>
    </Drawer>
  )
};