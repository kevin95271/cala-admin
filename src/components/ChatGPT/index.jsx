import { useState } from 'react';
import { Layout, Input, Card, Avatar, Button, Form, Row, Col } from 'antd';
import { CommentOutlined, UserOutlined, SlackOutlined } from '@ant-design/icons';
import DragModal from '@/components/DragModal';
import FloatButton from '@/components/FloatButton';
import './index.less';
const { Footer, Content } = Layout;

export default (props) => {
    const [form] = Form.useForm();
    const [modalOpen, setModalOpen] = useState(false);
    const { TextArea } = Input;
    // const question = '今天青岛天气怎么样？';
    // const answer = '我的知识截止到2021年9月，因此无法提供当前的天气信息。建议你通过天气预报网站、手机应用或当地的气象部门获取最新的青岛天气预报。';
    // const [chatList, setChatList] = useState([{ role: 'user', content: '今天青岛天气怎么样？', createTime: '' }, { role: 'assistant', content: '建议你通过天气预报网站、手机应用或当地的气象部门获取最新的青岛天气预报。', createTime: '' }]);
    const [chatList, setChatList] = useState([]);

    const onFinish = (e) => {
        console.log(e);
        // setChatList([]);
        let question = [];
        question = chatList.concat([{ role: 'user', content: e.question, createTime: '' }]);
        // question.push({ role: 'user', content: e.question, createTime: '' })
        setChatList(question);
        form.resetFields();
    };

    return (<>
        {/* <FloatButton type="primary" tooltip="calaGPT" icon={<CommentOutlined />} onClick={() => setModalOpen(true)} /> */}
        <FloatButton  onClick={() => setModalOpen(true)} >
            <Button type="primary" shape="circle" icon={<CommentOutlined />} />
        </FloatButton>

        <DragModal
            title="CalaGPT"
            open={modalOpen}
            width={1000}
            bodyStyle={{ height: '500px' }}
            onCancel={() => setModalOpen(false)}
            footer={null}
        >
            <Layout >
                <Content style={{ height: 410, overflow: 'auto' }}>
                    <div className='chat-result'>
                        {chatList.map((item) =>
                            item.role == 'user' ? <div className='list-item question'>
                                <Card className='content'>
                                    {item.content}
                                </Card>
                                <Avatar
                                    className='header'
                                    icon={<UserOutlined />}
                                />
                            </div> : <div className='list-item answer'>
                                <Avatar
                                    className='header'
                                    icon={<SlackOutlined />}
                                />
                                <Card className='content'>
                                    {item.content}
                                </Card>
                            </div>
                        )}
                    </div>
                </Content>
                <Footer style={{ padding: '0' }}>
                    <div className='chat-ask'>
                        <Form
                            className='chat-form'
                            onFinish={onFinish}
                            form={form}
                        >
                            <Row wrap="no">
                                <Col flex="auto">
                                    <Form.Item label="" name="question">
                                        <TextArea className='question' placeholder="请输入您的问题" bordered={false} />
                                    </Form.Item>
                                </Col>
                                <Col flex="100px">
                                    <Button className='send' type="primary" size='large' htmlType="submit">发送</Button>
                                </Col>
                            </Row>
                        </Form>
                    </div>
                </Footer>
            </Layout>
        </DragModal>
    </>)
}
