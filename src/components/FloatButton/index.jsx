import { useState } from 'react';
import Draggable from 'react-draggable';
import './index.less';

export default (props) => {
    const { clientHeight } = window.document.documentElement;
    const [isDrag, setIsDrag] = useState(true);

    return (<>
        <Draggable
            bounds={{left:0,right:0,bottom:100,top:-(clientHeight-207)}}
            onDrag={() => { setIsDrag(false); }}
            onStop={() => {
                if (isDrag) {
                    props.onClick();
                }
                setIsDrag(true);
            }}
        >
            <div className='floatButton'>
                {props.children}
            </div>
        </Draggable>
    </>)
}
