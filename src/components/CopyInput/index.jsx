import { useRef } from 'react';
import { Input, Button, message } from 'antd';
import { CopyOutlined } from '@ant-design/icons';

export default (props) => {
    const inputRef = useRef();

    const copyToClipboard = () => {
        let value = inputRef.current.input.value;
        navigator.clipboard.writeText(value).then(
            () => {
                message.info("复制成功!")
            }
        )
    }

    return (<Input.Group compact>
        <Input
            ref={inputRef}
            value={props.value}
            onChange={props.onChange}
            style={props.size=='small'?{ width: 'calc(100% - 24px)' }:{ width: 'calc(100% - 32px)' }}
        />
        <Button icon={<CopyOutlined />} onClick={copyToClipboard} />
    </Input.Group>)
}
