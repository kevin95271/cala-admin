import { useState } from 'react';
import { Form, Row, Col, Divider, Input, Select, Tooltip, InputNumber, Button, Popconfirm } from 'antd';

const Shipper = (props) => {
    const [isVisible, setIsVisible] = useState(false);

    return (<Row>
        <Col span={24} style={{ display: 'flex', justifyContent: 'space-between' }}>
            <Form.Item label={props.label}></Form.Item>
            {props.isMark && <Button.Group>
                <Button value="10">10</Button>
                <Button value="20">20</Button>
                <Tooltip
                    open={isVisible}
                    trigger="click"
                    color='white'
                    title={<div style={{ width: '180px', padding: '10px 14px',display:'block'}}>
                        <Row>
                            <Col style={{ color: '#333333', paddingBottom: '10px' }}>自定义每行字符数</Col>
                        </Row>
                        <Row>
                            <Col flex="auto"><InputNumber placeholder="" min={20} style={{ width: '100px' }} /></Col>
                            <Col flex="50px">
                                <Button type="primary" onClick={()=>{setIsVisible(false)}}>
                                    确定
                                </Button>
                            </Col>
                        </Row>
                    </div>
                    }
                >
                    <Button value="diy" onClick={()=>{setIsVisible(!isVisible);}}>DIY</Button>
                </Tooltip >
            </Button.Group>}

            {!props.noBtns&&!props.isMark&&<Button.Group>
                <Button value="30">30</Button>
                <Button value="35">35</Button>
                <Button value="40">40</Button>
                <Tooltip
                    open={isVisible}
                    trigger="click"
                    color='white'
                    title={<div style={{ width: '180px', padding: '10px 14px',display:'block'}}>
                        <Row>
                            <Col style={{ color: '#999', paddingBottom: '10px' }}>自定义每行字符数</Col>
                        </Row>
                        <Row>
                            <Col flex="auto"><InputNumber placeholder="" min={20} style={{ width: '100px' }} /></Col>
                            <Col flex="50px">
                                <Button type="primary" onClick={()=>{setIsVisible(false)}}>
                                    确定
                                </Button>
                            </Col>
                        </Row>
                    </div>
                    }
                >
                    <Button value="diy" onClick={()=>{setIsVisible(!isVisible);}}>DIY</Button>
                </Tooltip >
            </Button.Group>}
        </Col>
        <Col span={24}>
            <Input.TextArea style={{ height: props.height || '120px', lineHeight: '17px', fontSize: '14px' }} onChange={props.onChange} />
        </Col>
    </Row>)

}

export default Shipper