import { history } from 'umi';
import { message } from 'antd';

message.config({top: 200,});

//TODO:TOKEN 验证，request 封装

const token = sessionStorage.getItem('token');

if (token) {
    history.push(history.location.pathname);
} else {
    history.push('/user/login');
}