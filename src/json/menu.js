export default [
    {
        "icon": "dashboard",
        "name": "工作台",
        "path": "/dashboard"
    },
    {    //表单   
        "icon": "form",
        "name": "表单页面",
        "path": "/form",
        "children": [
            {
                "name": "复杂表单",
                "path": "/form/multi"
            },
            {
                "name": "简单表单",
                "path": "/form/simple",
            }
        ]
    },
    {    //表格   
        "icon": "table",
        "name": "表格页面",
        "path": "/table",
        "children": [
            {
                "name": "普通表格",
                "path": "/table/table",
            },
            {
                "name": "新增表单",
                "path": "/table/table/add",
                "hideInMenu":true
            },
            {
                "name": "高级表格",
                "path": "/table/grid",
            }
        ]
    },
    {    // 电子签章   
        "icon": "signature",
        "name": "电子签章",
        "path": "/signature",
        "children": [
            {
                "name": "电子签章",
                "path": "/signature/seal"
            }
        ]
    },
    {
        "icon": "layout",
        "name": "拖动布局",
        "path": "/layout",
        "children": [
            {
                "name": "水平布局",
                "path": "/layout/vertical"
            },
            {
                "name": "垂直布局",
                "path": "/layout/horizontal"
            },
            {
                "name": "混合布局",
                "path": "/layout/mix"
            }
        ]
    },
    {    // 流程图   
        "icon": "flow",
        "name": "流程管理",
        "path": "/flow",
        "children": [
            {
                "name": "流程设计",
                "path": "/flow/design"
            }
        ]
    },
    {   //系统管理     
        "icon": "system",
        "name": "系统管理",
        "path": "/system",
        "children": [
            {   // 用户管理
                "name": "用户管理",
                "path": "/system/users"
            },
            {   // 角色管理
                "name": "角色管理",
                "path": "/system/roles"
            },
            {   // 权限管理         
                "name": "权限管理",
                "path": "/system/rights"
            },
            {   // 部门管理         
                "name": "部门管理",
                "path": "/system/departments"
            },
        ]
    },

];