import React, { useState, useEffect } from 'react';
import { ConfigProvider, Dropdown,  } from 'antd';
import ProLayout, { PageContainer } from '@ant-design/pro-layout';
import {
    DashboardOutlined, FormOutlined, TableOutlined, EditOutlined, GroupOutlined, SettingOutlined, BlockOutlined, ClusterOutlined,
    SyncOutlined, PicCenterOutlined, UngroupOutlined, PicLeftOutlined, PicRightOutlined,    
} from '@ant-design/icons';
import ChatGPT from '@/components/ChatGPT';
import defaultSettings from '../../config/defaultSettings';
import AvatarDropdown from '@/components/Layout/AvatarDropdown';
import SettingDrawer from '@/components/Layout/SettingDrawer';
import { Link, history } from 'umi';
import KeepAlive, { useAliveController } from 'react-activation';
import routeCache from '../../config/routerCache.js';
import menuData from '../json/menu';
import './index.less';

const tabListInit = [{ key: '/dashboard', tab: '工作台', closable: false }];

export default (props) => {
    const [settings, setSettings] = useState(localStorage.getItem("settings") == null ? defaultSettings : JSON.parse(localStorage.getItem("settings")));
    const [pathname, setPathname] = useState(props.location.pathname);
    const [isVisible, setIsVisible] = useState(false);
    const [tabList, setTabList] = useState(tabListInit);
    const [actionTab, setActionTab] = useState('');
    const { dropScope, refresh, clear } = useAliveController();
    const [isCollapsed, setIsCollapsed] = useState(false);

    useEffect(() => {
        // console.log(props.location.pathname)
        setPathname(props.location.pathname);
        // 按钮新建
        if(props.location.pathname!='/'){
            addTab(props.location);
        }else{
            history.push('/dashboard');
        } 
    }, [props.location.pathname]);

    const iconEnum = {
        dashboard: <DashboardOutlined />,
        form: <FormOutlined />,
        table: <TableOutlined />,
        signature: <EditOutlined />,
        layout: <GroupOutlined />,
        flow: <ClusterOutlined />,
        system: <SettingOutlined />,
        baseData: <BlockOutlined />
    };

    // TAB右键菜单
    const menuItems = [
        { key: '1', label: <span onClick={() => { refreshTab(actionTab) }}>刷新本页</span>, icon: <SyncOutlined /> },
        { key: '2', label: <span onClick={() => { closeAllTabs() }}>关闭所有</span>, icon: <PicCenterOutlined /> },
        { key: '3', label: <span onClick={() => { closeOtherTabs(actionTab) }}>关闭其他</span>, icon: <UngroupOutlined /> },
        { key: '4', label: <span onClick={() => { closeLeftTabs(actionTab) }}>关闭左边</span>, icon: <PicLeftOutlined /> },
        { key: '5', label: <span onClick={() => { closeRightTabs(actionTab) }}>关闭右边</span>, icon: <PicRightOutlined /> }
    ];

    // 添加标签
    const addTab = (addItem) => {
        setPathname(addItem.pathname);
        // 缓存页面
        let index = tabList.findIndex((item) => { return item.key == addItem.pathname });
        if (index < 0) {
            let newTabs = tabList.concat({ key: addItem.pathname, tab: routeCache[addItem.pathname] });
            setTabList(newTabs);
        }
    }

    // 关闭标签
    const editTabs = (key, action) => {
        if (action == 'remove') {
            let newPath = tabList[tabList.findIndex((item) => { return item.key == key }) - 1].key;
            if (pathname == key) {
                setPathname(newPath);
                history.replace(newPath);
            }
            let newTabs = tabList.filter((item) => { return item.key != key });
            setTabList(newTabs);
            dropScope(key);
        }
    }

    // 右键事件
    const refreshTab = (key) => {
        refresh(key);
    }
    const closeAllTabs = () => {
        let newTabs = [].concat(tabList[0]);
        setPathname(newTabs[0].key);
        history.replace(newTabs[0].key);
        setTabList(newTabs);
        clear();
    }
    const closeOtherTabs = (key) => {
        let index = tabList.findIndex((item) => { return item.key == key });
        let newTabs = [].concat(tabList[0]);
        newTabs = newTabs.concat(tabList[index]);
        setPathname(key);
        history.replace(key);
        setTabList(newTabs);
    }
    const closeLeftTabs = (key) => {
        let index = tabList.findIndex((item) => { return item.key == key });
        let newTabs = tabList.filter((_, itemIndex) => { return itemIndex >= index || itemIndex == 0 });
        setPathname(key);
        history.replace(key);
        setTabList(newTabs);
    }
    const closeRightTabs = (key) => {
        let index = tabList.findIndex((item) => { return item.key == key });
        let newTabs = tabList.filter((_, itemIndex) => { return itemIndex <= index });
        setPathname(key);
        history.replace(key);
        setTabList(newTabs);
    }
    // 菜单重构
    const menuDataRender = (menuList) => {
        return menuList.map((item) => {
            const localItem = {
                ...item,
                name: item.name,
                locale: true,
                icon: iconEnum[item.icon],
                routes: item.routes ? menuDataRender(item.routes) : undefined,
            };
            return localItem;
        });
    };

    return <ConfigProvider space={{ size: 'small' }}>
        <ProLayout
            {...settings}
            title='CaladogAdmin'
            location={{ pathname }}
            menu={{
                request: async () => {
                    return menuData;
                },
            }}
            avatarProps={{
                ////TODO 用户信息
                src: 'https://gw.alipayobjects.com/zos/antfincdn/efFD%24IOql2/weixintupian_20170331104822.jpg',
                size: 'small',
                title: '管理员',
                render: (_, dom) => {
                    return (<AvatarDropdown onSetting={() => { setIsVisible(true) }}>{dom}</AvatarDropdown>)
                }
            }}

            menuItemRender={(item, dom) => (
                location.pathname === item.path ? dom : <Link to={item.path} >{dom}</Link>
            )}
            menuDataRender={() => menuDataRender(menuData || [])}
            loading={false}
            // 自定义折叠 样式
            collapsed={isCollapsed}
            onCollapse={() => setIsCollapsed(!isCollapsed)}
            token={settings.navTheme == "light" && {
                header: {
                    colorBgHeader: 'rgba(250,250,250,0.6)',
                },
                sider: {
                    colorMenuBackground: 'rgba(250,250,250,0.2)',
                },
                pageContainer: {
                    colorBgPageContainer: 'rgba(255,255,255,0.8)'
                }
            }}
            className={settings.navTheme == "light" ? settings.theme : ""}
        >
            <PageContainer
                ghost
                header={settings.isTabs ? { title: '', breadcrumb: {} } : { title: '' }}
                waterMarkProps={{ content: 'Cala-Admin' }}
                tabList={settings.isTabs ? tabList : []}
                tabProps={{
                    type: 'editable-card',
                    size: 'small',
                    hideAdd: true,
                    tabBarGutter: 1,
                    tabBarStyle: { userSelect: 'none' },
                    activeKey: pathname,
                    onEdit: (v, action) => { editTabs(v, action) },
                    onChange: (v) => { history.replace(v) },
                    renderTabBar: (props, DefaultTabBar) =>
                        <DefaultTabBar {...props}>
                            {node => (
                                <Dropdown
                                    menu={{ items: menuItems }}
                                    trigger={['contextMenu']}
                                    onOpenChange={() => setActionTab(node.key)}
                                >
                                    {node}
                                </Dropdown>
                            )}
                        </DefaultTabBar>
                }}
                className={settings.isTabs ? 'cala-body tabPage' : 'cala-body breadcrumb'}
            >
                <div style={{ height: settings.layout == "side" ? 'calc(100vh - 64px)' : 'calc(100vh - 120px)', overflow: 'hidden' }}>
                    {settings.isTabs ? <KeepAlive name={pathname} id={pathname}>{props.children}</KeepAlive> : props.children}
                </div>                
            </PageContainer>
            <SettingDrawer
                visible={isVisible}
                settings={settings}
                onSettingChange={setSettings}
                closeDrawer={() => setIsVisible(false)}
            />
            <ChatGPT/>
        </ProLayout>
    </ConfigProvider>
}