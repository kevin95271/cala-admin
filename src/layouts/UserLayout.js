import { Helmet, HelmetProvider } from 'react-helmet-async';
import bgImage from '@/assets/images/loginbg.jpg';

export default (props) => {

    const container = {
        background: `url(${bgImage}) center bottom / cover no-repeat`,
        height: '100vh'
    }

    return (<HelmetProvider>
        <Helmet>
            <title>系统登录</title>
        </Helmet>
        <div style={container}>
            {props.children}
        </div>
    </HelmetProvider>)
}