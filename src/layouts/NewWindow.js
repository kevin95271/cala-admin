
import { Helmet, HelmetProvider } from 'react-helmet-async';

export default (props) => {
    //console.log(props)
    let url = props.location.pathname;
    let route = props.route.routes[0].path;
    let params = route.split(":");
    url = url.replace(params[0], "");
    let paramsArr = url.split("/");

    return (<HelmetProvider>
        <Helmet>
            <title>{paramsArr[1]} {paramsArr[0] == 'add' ? '' : paramsArr[0]}</title>
        </Helmet>
        {props.children}
    </HelmetProvider>)
}