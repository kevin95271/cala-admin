import { Button, Select, Space } from 'antd';
import ProTable from '@/components/ResizableProTable';
import { history } from 'umi';
import { NewWindow } from '@/utils/NewWindow';

export default (props) => {
  // const intl = useIntl();

  const columns = [
    {
      title: 'index',
      dataIndex: 'index',
      width: 60,
      valueType: 'indexBorder',
    },
    {
      title: 'Name',
      width: 200,
      dataIndex: 'name',
      copyable: true,
      ellipsis: true,
    },
    {
      title: 'Money',
      dataIndex: 'title',
      width: 100,
      valueType: 'money',
      renderText: () => (Math.random() * 100).toFixed(2),
    },
    {
      title: 'Creator',
      key: 'creator',
      width: 100,
      dataIndex: 'creator',
    },
    {
      title: 'Created Time',
      key: 'since',
      dataIndex: 'createdAt',
      valueType: 'dateTime',
    },
    {
      title: 'Action',
      key: 'action',
      render: (_, record) => (
        <Button key={record.name} size="small" onClick={() => { NewWindow('/new/users/add/' + record.name + '/提单详情') }}>
          编辑信息
        </Button>
      ),
    },
  ];

  const tableListDataSource = [];

  for (let i = 0; i < 255; i += 1) {
    tableListDataSource.push({
      key: i,
      name: 'AppName' + i,
      containers: Math.floor(Math.random() * 20),
      createdAt: Date.now() - Math.floor(Math.random() * 2000),
      creator: 'Sunny',
    });
  }

  const dataList = () => {
    return {
      data: tableListDataSource,
      success: true,
    };
  }


  const { offsetHeight } = window.document.getElementsByClassName("cala-body")[0]; //获取容器高度

  return (
    <ProTable
      rowKey="key"
      defaultSize="small"
      bordered
      scroll={{ x: 1600, y: offsetHeight - 322 }}
      columns={columns}
      request={dataList}
      rowSelection={{}}
      search={{ labelWidth: 'auto', }}
      dateFormatter="string"
      headerTitle={<span>角色管理</span>}
      toolBarRender={() => [
        <Button key="3" type="primary" onClick={() => { NewWindow('/new/users/add/add/新建提单') }}>
          新建
        </Button>
      ]}
      pagination={{ size: 'small' }}
    />
  );
}
