import React, { useState, useMemo } from 'react';

import DragLayout from '@/components/DragLayout';

export default () => {


  const { clientHeight } = window?.document?.documentElement;

  return (
    <DragLayout containerHeight={clientHeight - 120} layout="vertical" >
      <div style={{ height: '100%', border: '1px solid #f0f0f0', background: '#fff' }}>
        left
      </div>
      <div style={{ height: '100%', border: '1px solid #f0f0f0', background: '#fff' }}>
        right
      </div>
    </DragLayout>
  );
};