import React, { useState, useEffect } from "react";
import { Line, Bar, Liquid, Histogram } from '@ant-design/charts';
// 测试数据
const config3 = {
  percent: 0.35,
  height: 250,
};

// 年度单量 TEU 
export default (props) => {
    return (<Liquid {...config3}  style={{height:"100%"}} />)
}