import React, { useState, useEffect } from "react";
import { Card, Typography, Row, Col, Button } from 'antd';
import { Column } from '@ant-design/charts';

export default () => {
  const data = [
    {
      name: 'BL',
      month: 'Jan',
      value: 189,
    },
    {
      name: 'BL',
      month: 'Feb',
      value: 288,
    },
    {
      name: 'BL',
      month: 'Mar',
      value: 393,
    },
    {
      name: 'BL',
      month: 'Apr',
      value: 564,
    },
    {
      name: 'BL',
      month: 'May',
      value: 47,
    },
    {
      name: 'BL',
      month: 'Jun',
      value: 0,
    },
    {
      name: 'BL',
      month: 'Jul',
      value: 0,
    },
    {
      name: 'BL',
      month: 'Aug',
      value: 0,
    },
    {
      name: 'BL',
      month: 'Sep',
      value: 0,
    },
    {
      name: 'BL',
      month: 'Oct',
      value: 0,
    },
    {
      name: 'BL',
      month: 'Nov',
      value: 0,
    },
    {
      name: 'BL',
      month: 'Des',
      value: 0,
    },
    {
      name: 'TEU',
      month: 'Jan',
      value: 124,
    },
    {
      name: 'TEU',
      month: 'Feb',
      value: 232,
    },
    {
      name: 'TEU',
      month: 'Mar',
      value: 345,
    },
    {
      name: 'TEU',
      month: 'Apr',
      value: 907,
    },
    {
      name: 'TEU',
      month: 'May',
      value: 126,
    },
    {
      name: 'TEU',
      month: 'Jun',
      value: 0,
    },
    {
      name: 'TEU',
      month: 'Jul',
      value: 0,
    },
    {
      name: 'TEU',
      month: 'Aug',
      value: 0,
    },
    {
      name: 'TEU',
      month: 'Sep',
      value: 0,
    },
    {
      name: 'TEU',
      month: 'Oct',
      value: 0,
    },
    {
      name: 'TEU',
      month: 'Nov',
      value: 0,
    },
    {
      name: 'TEU',
      month: 'Des',
      value: 0,
    },
  ];
  const config = {
    data,
    isGroup: true,
    xField: 'month',
    yField: 'value',
    seriesField: 'name',
    dodgePadding: 1,

    legend: {
      position: 'top-right',
    }
  };
  return <>
    <div style={{ position: 'absolute', top: '5px', left: '10px', fontSize: '16px' }}>业务统计</div>
    <Column {...config} style={{height:"100%"}}/>
  </>;
};