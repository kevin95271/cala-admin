import { Card, List, Button } from 'antd';
import { EyeOutlined } from '@ant-design/icons';
import moment from 'moment';

export default () => {
    let taskData = [{
        "id": "1649288428741824514",
        "message": "费用待确认<br/>QINGDAO - HAMBURG <br/> 40HC*2 2023-05-10 ",
        "receiveUserNo": "QuinnWang",
        "state": "UNREAD",
        "title": "[订舱] 您的订单BO2304001",
        "messageType": "待办",
        "createTime": "2023-04-21T13:46:18.000+0800"
    }, {
        "id": "1649287816285360129",
        "message": "请尽快确认提单信息",
        "receiveUserNo": "QuinnWang",
        "state": "UNREAD",
        "title": "[截单] 您的提单6305843240",
        "messageType": "待办",
        "createTime": "2023-04-21T13:43:52.000+0800"
    }, {
        "id": "1649278386089070594",
        "message": "USD发票已开出请查收，<br/>15404628512142112345.pdf",
        "receiveUserNo": "QuinnWang",
        "state": "UNREAD",
        "title": "[发票] 您的发票 IA230400001",
        "messageType": "待办",
        "createTime": "2023-04-21T13:06:24.000+0800",
        "toUrl": "/operate/paymentApply"
    }, {
        "id": "1649287816285360129",
        "message": "请尽快确认提单信息",
        "receiveUserNo": "QuinnWang",
        "state": "UNREAD",
        "title": "[截单] 您的提单6305843240",
        "messageType": "待办",
        "createTime": "2023-04-21T13:43:52.000+0800"
    }];

    return (
        <Card
            size="small"
            title={<div style={{ fontSize: '16px' }}>待办任务</div>}
            extra={<a href="#">更多</a>}
            bordered={false}
        >
            <List
                size='small'
                dataSource={taskData}
                renderItem={(item, index) => (
                    <List.Item>
                        <List.Item.Meta
                            key={index}
                            avatar={<Button size='small' type='text' icon={<EyeOutlined />} />}
                            title={item.title}
                            description={<><p style={{ marginBottom: 0,fontSize:'12px',lineHeight:'18px' }} dangerouslySetInnerHTML={{ __html: item.message }}></p>{moment(item.createTime).format("yyyy-MM-DD HH:mm:ss")}</>}
                        />
                    </List.Item>
                )}
            />
        </Card>);
}
