import React, { useState, useEffect } from "react";
import { Line, Bar, Liquid, Histogram } from '@ant-design/charts';
// 测试数据
const config2 = {
  data: [{ year: '1991', value: 3 }, { year: '1992', value: 4 }, { year: '1993', value: 3.5 }, { year: '1994', value: 5 }, { year: '1995', value: 4.9 }, { year: '1996', value: 6 }, { year: '1997', value: 7 }],
  xField: 'year',
  yField: 'value',
  point: {
    size: 5,
    shape: 'diamond',
  },
};

// 年度单量 TEU 
export default (props) => {
    return (<Line {...config2} style={{height:"100%"}} />)
}