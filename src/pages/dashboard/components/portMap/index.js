import React, { useState, useEffect } from "react";
import { Typography, theme } from 'antd';
import { Map, APILoader, ScaleControl, ToolBarControl, Geolocation, Marker } from "@uiw/react-amap";
import './index.less';

// 港口地图
export default (props) => {
  const { token: { colorBgBase } } = theme.useToken();// 主题切换

  const ports = [
    {
      id: "1000001",
      state: "未开船",
      number: 24,
      port: "QINGDAO",
      position: [120.22, 36.02]
    },
    {
      id: "1000002",
      state: "航行中",
      number: 116,
      port: "",
      position: [179.00, 45.00]
    },
    {// 洛杉矶
      id: "1000003",
      state: "已到港",
      number: 16,
      port: "LOS ANGELES",
      position: [-113.25, 33.75]
    },
    {// 蒙巴萨
      id: "1000004",
      state: "已到港",
      number: 6,
      port: "MOMBASA",
      position: [9.85, 2.23]
    },
    {// 汉堡
      id: "1000004",
      state: "已到港",
      number: 33,
      port: "HAMBURG",
      position: [10.00, 53.55]
    },

  ]


  return (<APILoader version="2.0.5" akey="9b52a4cb861d3ed69ec17bb08dc01cdf">
    <div style={{ position: 'absolute', top: '15px', left: '20px', zIndex: 999 }}>
      <Typography.Text style={{fontSize: '16px'}}>实时货物状态</Typography.Text>
      <span style={{ fontSize: '12px', color: '#f0b000' }}> [按箱计数]</span>
    </div>
    <Map
      mapStyle={colorBgBase == '#fff' ? "amap://styles/whitesmoke" : "amap://styles/dark"}
      zoom={1}
      style={{ height: 'calc(100% + 20px)' }}>
      <ScaleControl offset={[10, 30]} position="LB" />
      <ToolBarControl offset={[10, 30]} position="RB" />
      <Geolocation
        maximumAge={10000}
        borderRadius="5px"
        position="RT"
        offset={[10, 10]}
        zoomToAccuracy={true}
        showCircle={true}
      />
      {ports.map((item) => <Marker position={item.position} offset={[-30, -60]} key={item.id} >
        <div className={item.number < 100 ? "bill-marker scale60" : "bill-marker"} title={item.port}>
          <div className="bill-number">{item.number}</div>
          <div className="bill-state">{item.state}</div>
        </div>
      </Marker>)
      }

    </Map>
  </APILoader>)
}