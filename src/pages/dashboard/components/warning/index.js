import { Card, List, Badge, Avatar } from 'antd';
import { ExclamationCircleOutlined } from '@ant-design/icons';

export default () => {
    let warnData = [{
        "id": "1649288428741824514",
        "count": 0,
        "title": "[拖班]",
        "message":"提单6295843210"
    }, {
        "id": "1649287816285360129",
        "count": 3,
        "title": "[放行异常]",
        "message":"提单14030584320 ..."
    }, {
        "id": "1649278386089070594",
        "count": 0,
        "title": "[退关]",
        "message":"提单6305849870"
    }, {
        "id": "1649287816285360188",
        "count": 12,
        "title": "[截港未运抵]",
        "message":"提单6305843240 ..."
    }];

    return (
        <Card
            size="small"
            title={<div style={{ fontSize: '16px' }}>异常提醒</div>}
            extra={<a href="#">全部</a>}
            bordered={false}
        >
            <List
                size='small'
                dataSource={warnData}
                renderItem={(item, index) => (
                    <List.Item>
                        <List.Item.Meta
                            key={index}
                            avatar={<Badge size="small" count={item.count} overflowCount={9}>
                                <Avatar
                                    shape="square"
                                    icon={<ExclamationCircleOutlined />}
                                    style={{
                                        backgroundColor: '#f0b000',
                                    }}
                                />
                            </Badge>}
                            title={item.title}
                            description={<p style={{ marginBottom: 0,fontSize:'12px',lineHeight:'18px' }} dangerouslySetInnerHTML={{ __html: item.message }}></p>}
                        />
                    </List.Item>
                )}
            />
        </Card>);
}
