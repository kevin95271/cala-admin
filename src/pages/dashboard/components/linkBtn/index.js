import { Card, Typography, Row, Col } from 'antd';
import { ScheduleOutlined, AimOutlined, PropertySafetyOutlined, FileTextOutlined } from '@ant-design/icons';
import './index.less';


export default () => {
    const Btn = (props) => {
        return <div className='link-btn'>
            <div className='btn-icon' style={{ background: props.bgcolor || '' }}>
                {props.children}
            </div>
            <div className='btn-name'>
                {props.linkName}
            </div>
        </div>
    }

    return (<Card
        size="small"
        title={<div style={{ fontSize:'16px' }}>快捷入口</div>}
        extra={<a href="#">管理</a>}
        bordered={false}
    >
        <Row gutter={[0,15]}>
            <Col span={6}>
                <Btn linkName="船期查询" bgcolor="#23BCEE">
                    <ScheduleOutlined />
                </Btn>
            </Col>
            <Col span={6}>
                <Btn linkName="船舶轨迹" bgcolor="#617FEE">
                    <AimOutlined />
                </Btn>
            </Col>
            <Col span={6}>
                <Btn linkName="账单查询" bgcolor="#188FFB">
                    <PropertySafetyOutlined />
                </Btn>
            </Col>
            <Col span={6}>
                <Btn linkName="单证管理" bgcolor="#23A937">
                    <FileTextOutlined />
                </Btn>
            </Col>
            <Col span={6}>
                <Btn linkName="账单查询" bgcolor="#188FFB">
                    <PropertySafetyOutlined />
                </Btn>
            </Col>
            <Col span={6}>
                <Btn linkName="单证管理" bgcolor="#23A937">
                    <FileTextOutlined />
                </Btn>
            </Col>
        </Row>
    </Card>);
}
