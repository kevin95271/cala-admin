import React, { useState, useEffect } from 'react';
import { Card, Form, Row, Col, Divider, Input, Button, AutoComplete, Modal, Select, DatePicker, Switch, Upload, Layout, List, Avatar, Typography, Checkbox, Tabs, message } from 'antd';
import { UploadOutlined, PlusOutlined, CopyOutlined } from '@ant-design/icons';
import Shipper from '@/components/TextArea';
import CopyInput from '@/components/CopyInput';
const { Sider, Content, Footer } = Layout;
import DragLayout from '@/components/DragLayout';
const { Text } = Typography;
const { TextArea } = Input;

import { saveAs } from 'file-saver';

export default (props) => {
  const [modelShow, setModelShow] = useState(false);
  const [form] = Form.useForm();
  const [isDanger, setIsDanger] = useState(false);
  const [isReefer, setIsReefer] = useState(false);
  const [approveShow, setApproveShow] = useState(false);
  const [activeKey, setActiveKey] = useState("b");

  const [messageForm] = Form.useForm();
  ///// 临时对话数据
  const [listData, setListData] = useState([]);

  const sendMessage = (e) => {
    // console.log(e);
    if (e.message) {
      let temp = [];
      temp.push({
        id: Date.now(),
        identity: '操作',
        cnName: '王五',
        dateTime: '2022-12-28 12:43',
        note: e.message,
        noteFlag: e.messageFlag
      });
      temp = temp.concat(listData);
      setListData(temp);
      console.log(temp);

      messageForm.resetFields();
    } else {
      message.warning("请输入要备注的信息！")
    }
  };


  const onFinish = (e) => {
    console.log(e);
  };

  const changeValue = (e) => {
    console.log(e.target.value);
  }

  const topValue = [
    { value: 'text 111', },
    { value: 'text 222', },
    { value: 'text 333', },
  ]

  // 转大写
  const handleChange = (event) => {
    // console.log(event.target.value)
    // setHelloTo(event.target.value.toUpperCase());
    form.setFieldsValue({ upperText: event.target.value.toUpperCase() })
  }

  //改变货物类型onFinish
  const handleChangeCargoType = (selectItem) => {
    setIsDanger(selectItem === "DANGER");
    setIsReefer(selectItem === "REEFER");
  };

  // demo
  const pdfUrls = [
    'https://www.chinaeinv.com:943/pdf.jspa?c=09AFC99573909A15FA9F',
    'https://www.chinaeinv.com:943/pdf.jspa?c=E6A1F361F21A20E2C16C',
    'https://www.chinaeinv.com:943/pdf.jspa?c=E347A438B6F7F471F291'
  ]

  // 下载文件
  const downloadPdf = (url, filename) => {
    const link = document.createElement('a');
    link.href = url;
    link.download = filename;
    // link.download = url.substring(url.lastIndexOf('/') + 1);
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  // 批量下载
  const downLoad = () => {
    pdfUrls.forEach((url, index) => {
      // setTimeout(() => {
      //   downloadPdf(url, `file${index + 1}.pdf`);        
      // }, 1000 * index);
      saveAs(url, `file${index + 1}.pdf`);
    });
  }

  // 放单通知
  const [noticeShow, setNoticeShow] = useState(false);


  const { clientHeight } = window?.document?.documentElement;

  return (<Card >
    <Row>
      <Col span={12}>
        <Form
          name="wrap"
          labelCol={{ flex: '120px' }}
          labelAlign="right"
          labelWrap
          wrapperCol={{ flex: 1 }}
          onFinish={onFinish}
          form={form}
          initialValues={{ userName: 'Quinn', passWord: 'asdfg' }}
        >
          <Form.Item label="正常标签" name="userName" rules={[{ required: true }]}>
            <Input />
          </Form.Item>

          <Form.Item label="自动大写" name="upperText" onBlur={handleChange}>
            <Input />
          </Form.Item>

          <Form.Item label="复制文本" name="passWord">
            <CopyInput />
          </Form.Item>

          <Form.Item label="自动提示" name="topMessage">
            <AutoComplete
              style={{
                width: '100%',
              }}
              placeholder="置顶提示"
              options={topValue}
              onBlur={changeValue}
              filterOption={true}
            >
            </AutoComplete>
          </Form.Item>

          <Form.Item label=" ">
            <Button type="primary" htmlType="submit">
              提 交
            </Button>
          </Form.Item>
        </Form>
      </Col>
    </Row>

    <Row>
      <Col span={4}>
        <Button type="primary" onClick={() => setModelShow(true)}>
          新建订舱
        </Button>
      </Col>
      {/* <Col span={4}>
        <Button type="primary" onClick={downLoad}>
          批量下载
        </Button>
      </Col> */}
      <Col span={4}>
        <Button type="primary" onClick={() => setApproveShow(true)}>
          付费申请
        </Button>
      </Col>
      <Col span={4}>
        <Button type="primary" onClick={() => setNoticeShow(true)}>
          放单通知
        </Button>
      </Col>
    </Row>

    <Modal title="订舱信息" centered width={1400} styles={{body:{ height: "calc(100vh - 194px)", overflow: 'auto', padding: '0' }}} open={modelShow} onCancel={() => setModelShow(false)}>
      <Layout style={{ height: "calc(100vh - 200px)", background: "transparent" }}>
        <Content style={{ padding: '15px 10px', borderRight: '1px solid #ccc' }}>
          <Form className="cala-form" form={form} size='small'>
            <Row style={{ display: 'none' }}>
              <Form.Item name="id" label="id"><Input /></Form.Item>
            </Row>
            <Row>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='entrustCustomerId' label="委托单位" className="dsi-required">
                  <Select />
                </Form.Item>
              </Col>
              <Col span={4}>
                <Form.Item labelCol={{ flex: '65px' }} label="收发通" name='sft' >
                  <Select />
                </Form.Item>
              </Col>
              <Col span={3}>
                <Form.Item labelCol={{ flex: '65px' }} name='entrustCustomerContact' label="联系人">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={5}>
                <Form.Item labelCol={{ flex: '80px' }} name='entrustCustomerPhone' label="联系电话">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='customerCode' label="客户约号">
                  <Input />
                </Form.Item>
              </Col>
            </Row>
            <Divider style={{ margin: '10px 0' }} />
            <Row gutter={8}>
              <Col span={8}>
                <Form.Item name='shipper' label="">
                  <Shipper label="Shipper(发货人)" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='consignee' label="">
                  <Shipper label="Consignee(收货人)" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='notify' label="">
                  <Shipper label="Notify Party(通知人)" />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='polId' label="装货港" className="dsi-required">
                  <Select />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='podId' label="卸货港" className="dsi-required">
                  <Select />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='pod' label="目的地">
                  <Select />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='etd' label="船期ETD" className="dsi-required">
                  <DatePicker style={{ width: '100%' }} />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='HScode' label="品名" className="dsi-required">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Row>
                  <Col span={15}>
                    <Form.Item labelCol={{ flex: '80px' }} name='cargoPkgs' label="件数包装" className="dsi-required">
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col span={9}>
                    <Form.Item name='cargoUnits'>
                      <Select>
                        <Option>PACKAGE</Option>
                      </Select>
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='podId' label="重量" className="dsi-required">
                  <Input suffix="KGS" />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='cargoSize' label="尺码" className="dsi-required">
                  <Input suffix="CBM" />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='shipCompanyId' label="船公司">
                  <Select type="tag" />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='signWayCode' label="签单方式">
                  <Select />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='companyCode' label="公司约号">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={2}>
                <Form.Item labelCol={{ flex: '80px' }} name='isSoc' label="是否SOC" valuePropName='checked'>
                  <Switch />
                </Form.Item>
              </Col>
              <Col span={4}>
                <Form.Item labelCol={{ flex: '90px' }} name='freeTime' label="箱使天数">
                  <Input />
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='cargoTypeCode' label="货物标识">
                  <Select onChange={handleChangeCargoType}>
                    <Option value="ORDINARY">普通货</Option>
                    <Option value="CHEMICAL">化工品</Option>
                    <Option value="DANGER">危险品</Option>
                    <Option value="REEFER">冻柜</Option>
                    <Option value="LIQUID">液袋</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='shipClauseCode' label="运输条款">
                  <Select />
                </Form.Item>
              </Col>
              <Col span={3}>
                <Form.Item labelCol={{ flex: '80px' }} name='shipPaymentCode' label="付费方式">
                  <Select>
                    <Option value="PP">预付</Option>
                    <Option value="PC">到付</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={3}>
                <Form.Item labelCol={{ flex: '80px' }} name='transportType' label="装运方式">
                  <Select>
                    <Option value="0">整箱</Option>
                    <Option value="1">拼箱</Option>
                  </Select>
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='ctnNumber' label="箱型箱量">
                  <Input />
                </Form.Item>
              </Col>

            </Row>
            {/* 危险品 */}
            {isDanger &&
              <Row>
                <Col span={5}>
                  <Form.Item labelCol={{ flex: '80px' }} name='cargoDangerLevel' label="危险品等级">
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={5}>
                  <Form.Item labelCol={{ flex: '100px' }} name='cargoDangerCode' label="危险品编号" >
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={5}>
                  <Form.Item labelCol={{ flex: '100px' }} name='cargoDangerPageNo' label="危险品页码" >
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={5}>
                  <Form.Item labelCol={{ flex: '100px' }} name='cargoDangerLabel' label="危险品标签" >
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={4}>
                  <Form.Item labelCol={{ flex: '110px' }} name='cargoDangerContact' label="危险品联系人" >
                    <Input />
                  </Form.Item>
                </Col>
              </Row>}
            {/* 冻柜 */}
            {isReefer &&
              <Row>
                <Col span={4}>
                  <Form.Item labelCol={{ flex: '80px' }} name='cargoFreezeWindow' label="通风量">
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={4}>
                  <Form.Item labelCol={{ flex: '80px' }} name='cargoFreezeHumidity' label="湿度" >
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={4}>
                  <Form.Item labelCol={{ flex: '80px' }} name='cargoFreezeTemperature' label="设置温度" >
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={4}>
                  <Form.Item labelCol={{ flex: '80px' }} name='cargoFreezeTemperatureUnit' label="温度单位" >
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={4}>
                  <Form.Item labelCol={{ flex: '80px' }} name='cargoFreezeTemperatureMin' label="最低温度" >
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={4}>
                  <Form.Item labelCol={{ flex: '80px' }} name='cargoFreezeTemperatureMax' label="最高温度" >
                    <Input />
                  </Form.Item>
                </Col>
              </Row>}
            <Divider style={{ margin: '10px 0' }} />
            {/* 报价 */}
            <Row>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='' label="箱型">
                  <b>20GP</b>
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='' label="箱型">
                  <b>40GP</b>
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='' label="箱型">
                  <b>40HC</b>
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='OTHER' label="箱型">
                  <Select style={{ width: '50%' }} placeholder="OTHER">
                    <Option>45GP</Option>
                    <Option>40RF</Option>
                    <Option>20RF</Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            <Row>
              <Col span={3}>
                <Form.Item labelCol={{ flex: '80px' }} name='GP20num' label="数量">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={3}>
                <Form.Item labelCol={{ flex: '60px' }} name='GP20prise' label="价格">
                  <Input suffix="$" />
                </Form.Item>
              </Col>
              <Col span={3}>
                <Form.Item labelCol={{ flex: '80px' }} name='PG40num' label="数量">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={3}>
                <Form.Item labelCol={{ flex: '60px' }} name='GP40prise' label="价格">
                  <Input suffix="$" />
                </Form.Item>
              </Col>
              <Col span={3}>
                <Form.Item labelCol={{ flex: '80px' }} name='HC40num' label="数量">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={3}>
                <Form.Item labelCol={{ flex: '60px' }} name='HC40prise' label="价格">
                  <Input suffix="$" />
                </Form.Item>
              </Col>
              <Col span={3}>
                <Form.Item labelCol={{ flex: '80px' }} name='OTHERnum' label="数量">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={3}>
                <Form.Item labelCol={{ flex: '60px' }} name='OTHERprise' label="价格">
                  <Input suffix="$" />
                </Form.Item>
              </Col>
            </Row>
            <Divider style={{ margin: '10px 0' }} />
            <Row gutter={8}>
              <Col span={6}>
                <Form.Item name='marks' label="">
                  <Shipper label="唛头" isMark={true} />
                </Form.Item>
              </Col>
              <Col span={10}>
                <Form.Item name='cargoDescription' label="">
                  <Shipper label="货描" />
                </Form.Item>
              </Col>
              <Col span={8}>
                <Form.Item name='note' label="">
                  <Shipper label="备注" noBtns={true} />
                </Form.Item>
              </Col>
            </Row>

            <Row gutter={8}>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='sellerCode' label="业务" className="dsi-required">
                  <Select />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='lineSupporterCode' label="航线客服" className="dsi-required">
                  <Select />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='operatorCode' label="操作" className="dsi-required">
                  <Select />
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item labelCol={{ flex: '80px' }} name='overseasSupporterCode' label="海外客服">
                  <Select />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={8}>
              <Col span={24}>
                <Form.Item labelCol={{ flex: '80px' }} label="附件上传">
                  <Upload multiple={true} listType="picture-card">
                    <PlusOutlined />
                    选择文件
                  </Upload>
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </Content>
        <Sider width={200} >
          <Layout style={{ height: '100%' }}>
            <Content style={{ padding: '10px', overflow: 'auto' }}>
              <List
                itemLayout="vertical"
                dataSource={listData}
                renderItem={(item) => (
                  <div key={item.id} style={{ borderRadius: '5px', padding: '6px', marginBottom: '10px' }}>
                    <Row wrap={false}>
                      <Col flex='40px'><Avatar style={{ marginTop: '4px' }}>{item.identity}</Avatar></Col>
                      <Col flex='auto'>
                        <Row>
                          <Col span={24}>
                            <Text style={{ fontSize: '12px', lineHeight: '20px' }}>{item.cnName}</Text>
                          </Col>
                          <Col span={24}>
                            <Text type="secondary" style={{ fontSize: '12px', lineHeight: '20px' }}>{item.dateTime}</Text>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                    <Row>
                      <Col>
                        <Text type={item.noteFlag ? 'danger' : ''} style={{ fontSize: '12px', lineHeight: '20px', margin: '10px' }}>{item.note}</Text>
                      </Col>
                    </Row>
                  </div>
                )}
              />
            </Content>
            <Footer
              style={{
                position: 'sticky',
                bottom: 0,
                padding: '10px'
              }}
            >
              <Form
                form={messageForm}
                onFinish={sendMessage}
              >
                <Row>
                  <Col span={24}>
                    <Form.Item name='message' style={{ margin: 0, padding: 0 }}>
                      <TextArea style={{ height: '100px', resize: 'none' }} />
                    </Form.Item>
                  </Col>
                </Row>
                <Row>
                  <Col flex='auto'>
                    <Form.Item name='messageFlag' valuePropName="checked" style={{ margin: 0, padding: 0 }}>
                      <Checkbox>重要</Checkbox>
                    </Form.Item>
                  </Col>
                  <Col flex='50px'>
                    <Form.Item style={{ margin: 0, padding: 0, marginTop: '2px' }}>
                      <Button size='small' type="primary" htmlType='submit'>发送</Button>
                    </Form.Item>
                  </Col>
                </Row>
              </Form>
            </Footer>
          </Layout>
        </Sider>
      </Layout>
    </Modal>

    <Modal
      title={<Tabs type="card" defaultActiveKey={activeKey} items={[{ label: "申请信息", key: "a", }, { label: "发票信息", key: "b", }, { label: "付款方信息", key: "c", }]} onChange={(e) => { setActiveKey(e) }} tabBarExtraContent={{ left: <span style={{ marginRight: '20px', fontSize: '16px' }}>新建付费申请</span> }} />}
      centered width={1600}
      styles={{ body: { height: "calc(100vh - 196px)", overflow: 'auto', padding: '0', marginTop: '-15px' } }}
      open={approveShow}
      onCancel={() => setApproveShow(false)}
    >
      <Form className="cala-form" form={form} size='small'>
        <Row gutter={10}>
          <Col span={12}>
            <Divider orientation='left' plain style={{ margin: '0 0 10px 0', fontSize: '12px' }}>基本信息</Divider>
            <Row>
              <Col span={8}>
                <Row>
                  <Col span={24}>
                    <Form.Item labelCol={{ flex: '80px' }} name='code' label="申请编号">
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col span={24}>
                    <Form.Item labelCol={{ flex: '80px' }} name='type' label="申请类型">
                      <Select options={[{ value: '0', label: '到票付款', }, { value: '1', label: '先付后补', }, { value: '2', label: '无票申请', }]} />
                    </Form.Item>
                  </Col>
                  <Col span={24}>
                    <Form.Item labelCol={{ flex: '80px' }} name='approver' label="申 请 人">
                      <Select />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col span={8}>
                <Row>
                  <Col span={24}>
                    <Form.Item labelCol={{ flex: '80px' }} name='company' label="结算单位">
                      <Select />
                    </Form.Item>
                  </Col>
                  <Col span={24}>
                    <Form.Item labelCol={{ flex: '80px' }} name='bb' label="申请币别">
                      <Select options={[{ value: '0', label: '原币申请', }, { value: '1', label: 'RMB', }, { value: '2', label: 'USD', }]} />
                    </Form.Item>
                  </Col>
                  <Col span={24}>
                    <Form.Item labelCol={{ flex: '80px' }} name='method' label="付款方式">
                      <Select options={[{ value: '0', label: '汇款', }, { value: '1', label: '中远线上', }, { value: '2', label: '马士基线上', }]} />
                    </Form.Item>
                  </Col>
                </Row>
              </Col>
              <Col span={8}>
                <Form.Item labelCol={{ flex: '50px' }} name='note' label="备注">
                  <TextArea style={{ resize: 'none' }} rows={4} />
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={4}>
            <Divider orientation='left' plain style={{ margin: '0 0 10px 0', fontSize: '12px' }}>RMB</Divider>
            <Row>
              <Col span={24}>
                <Form.Item labelCol={{ flex: '50px' }} labelWrap={true} name='approver' label="金额">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item labelCol={{ flex: '50px' }} labelWrap={true} name='approver' label="银行">
                  <Select />
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item labelCol={{ flex: '50px' }} labelWrap={true} name='approver' label="账号">
                  <Select />
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={4}>
            <Divider orientation='left' plain style={{ margin: '0 0 10px 0', fontSize: '12px' }}>USD</Divider>
            <Row>
              <Col span={24}>
                <Form.Item labelCol={{ flex: '50px' }} labelWrap={true} name='approver' label="金额">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item labelCol={{ flex: '50px' }} labelWrap={true} name='approver' label="银行">
                  <Select />
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item labelCol={{ flex: '50px' }} labelWrap={true} name='approver' label="账号">
                  <Select />
                </Form.Item>
              </Col>
            </Row>
          </Col>
          <Col span={4}>
            <Divider orientation='left' plain style={{ margin: '0 0 10px 0', fontSize: '12px' }}>其他币别</Divider>
            <Row>
              <Col span={12}>
                <Form.Item labelCol={{ flex: '50px' }} labelWrap={true} name='approver' label="币别">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item labelCol={{ flex: '50px' }} labelWrap={true} name='approver' label="金额">
                  <Input />
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item labelCol={{ flex: '50px' }} labelWrap={true} name='approver' label="银行">
                  <Select />
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item labelCol={{ flex: '50px' }} labelWrap={true} name='approver' label="账号">
                  <Select />
                </Form.Item>
              </Col>
            </Row>
          </Col>
        </Row>
      </Form>
    </Modal>


    <Modal
      title="放单通知"
      centered
      width={800}
      open={noticeShow}
      onCancel={() => setNoticeShow(false)}
      styles={{ body: { height: '480px', padding: '20px 0' } }}
    >
      <Form className="cala-form" form={form}>
        <Row gutter={10}>
          <Col span={24}>
            <Form.Item label="发件人" name="sender" labelCol={{ flex: "65px" }}>
              <Input readOnly placeholder='发送者邮箱'/>
            </Form.Item>
          </Col>
        
          <Col span={24}>
            <Form.Item label="收件人" name="receiver" labelCol={{ flex: "65px" }}>
              <Input placeholder='销售邮箱'/>
            </Form.Item>
          </Col>

          <Col span={24}>
            <Form.Item label="抄送人" name="copyer" labelCol={{ flex: "65px" }}>
              <Input />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item label="主 题" name="subject" labelCol={{ flex: "65px" }}>
              <Input defaultValue="QDDS9807065-放单申请-未符合条件通知"/>
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item label="正 文" name="content" labelCol={{ flex: "65px" }}>
              <Input.TextArea rows={12}/>
            </Form.Item>
          </Col>

        </Row>
      </Form>
    </Modal>

  </Card>);
}
