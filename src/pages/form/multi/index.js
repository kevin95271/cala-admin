import React, { useState, useRef } from 'react';
import { Card, Form, Row, Col, Divider, Input, Select, Table, Switch, Button, Space, DatePicker, InputNumber } from 'antd';
import { SaveOutlined } from '@ant-design/icons';
import Shipper from '@/components/TextArea';
import dayjs from 'dayjs';
import CopyInput from '@/components/CopyInput';
import './index.less';

export default (props) => {
  const [isDanger, setIsDanger] = useState(false);
  const [isReefer, setIsReefer] = useState(false);

  const [isOpen, setIsOpen] = useState(false);
  const [tempDate, setTempDate] = useState('');

  const [formRef] = Form.useForm();
  const [items, setItems] = useState(['COSCO ALEIN', 'CMA CGM LINA']);
  const addItem = (form) => {
    console.log(form);
    setItems([...items, form.newVessel]);
    formRef.resetFields();
  };


  const changeTag = (e) => {
    setIsDanger(e === 'D');
    setIsReefer(e === 'R');
  }

  //! 测试数据
  const dataSource = [
    {
      key: '1',
      containerNo: 'DRYU6014637',
      sealNo: 'CNB813650',
      containerType: '40HC',
      cargoTotalQty: 123,
      cargoPkgName: 'CASES',
      cargoTotalWeight: 22000,
      containerSelfWeight: 23000,
      cargoTotalSize: 35.5,
      weightMethod: 'SM'
    },
    {
      key: '2',
      containerNo: 'DRYU6014638',
      sealNo: 'CNB813659',
      containerType: '40HC',
    },
    {
      key: '3',
      containerNo: 'DRYU6014631',
      sealNo: 'CNB813651',
      containerType: '40HC',
    },
    {
      key: '4',
      containerNo: 'DRYU6014648',
      sealNo: 'CNB813649',
      containerType: '40HC',
    },
    {
      key: '5',
      containerNo: 'DRYU6014538',
      sealNo: 'CNB813559',
      containerType: '40HC',
    },
    {
      key: '6',
      containerNo: 'DRYU6014638',
      sealNo: 'CNB813659',
      containerType: '40HC',
    },
    {
      key: '7',
      containerNo: 'DRYU6014638',
      sealNo: 'CNB813659',
      containerType: '40HC',
    },
    {
      key: '8',
      containerNo: 'DRYU6014638',
      sealNo: 'CNB813659',
      containerType: '40HC',
    },
    {
      key: '9',
      containerNo: 'DRYU6014638',
      sealNo: 'CNB813659',
      containerType: '40HC',
    },
    {
      key: '10',
      containerNo: 'DRYU6014638',
      sealNo: 'CNB813659',
      containerType: '40HC',
    },
  ];

  // 列属性
  const columns = [
    {
      title: '序号',
      width: 60,
      align: 'center',
      dataIndex: 'key',
    },
    {
      title: '箱号',
      width: 120,
      align: 'center',
      ellipsis: true,
      dataIndex: 'containerNo',
    },
    {
      title: '箱型',
      width: 80,
      align: 'center',
      dataIndex: 'containerType',
    },
    {
      title: '铅封号',
      width: 120,
      align: 'center',
      ellipsis: true,
      dataIndex: 'sealNo',
    },
    {
      title: '件数',
      width: 80,
      align: 'center',
      dataIndex: 'cargoTotalQty',
    },
    {
      title: '包装类型',
      width: 100,
      align: 'center',
      ellipsis: true,
      dataIndex: 'cargoPkgName',
    },
    {
      title: '货重(KGS)',
      width: 100,
      align: 'center',
      ellipsis: true,
      dataIndex: 'cargoTotalWeight',
    },
    {
      title: '皮重(KGS)',
      width: 100,
      align: 'center',
      ellipsis: true,
      dataIndex: 'containerSelfWeight',
    },
    {
      title: '尺码(CBM)',
      width: 100,
      align: 'center',
      ellipsis: true,
      dataIndex: 'cargoTotalSize',
    },
    {
      title: '称重方式',
      width: 100,
      align: 'center',
      dataIndex: 'weightMethod'
    },
    {
      title: '称重重量(KGS)',
      width: 120,
      align: 'center',
      ellipsis: true,
      dataIndex: 'cargoWeighingWeight',
    },
    {
      title: '联系人',
      width: 100,
      align: 'center',
      dataIndex: 'vgmContact',
    },
    {
      title: '称重时间',
      width: 120,
      align: 'center',
      dataIndex: 'weightDate',
      valueType: 'date',
    },
    {
      title: '装运方式',
      width: 100,
      align: 'center',
      dataIndex: 'shippingMethod'
    },
    {
      title: '备注',
      width: 100,
      align: 'center',
      ellipsis: true,
      dataIndex: 'note',
    },
  ];

  const onFinish = (values) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  return (<Card bodyStyle={{ overflow: 'auto', height: 'calc(100vh - 180px)' }}
    actions={[
      <Space style={{ width: '100%', justifyContent: 'flex-end', paddingRight: '30px' }}>
        <Button type="primary" htmlType="submit">
          保存
        </Button>
        <Button htmlType="button">
          重置
        </Button>
      </Space>
    ]
    }>
    <Form
      name="basic"
      initialValues={{ dps22: dayjs('2022-12-19') }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
      size='small'
      className='cala-form'
      scrollToFirstError={true}
    >
      <Row>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="委托编号" name="s01">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="主提单号" name="s02" rules={[{ required: true }]} className='cala-required'>
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="分提单号" name="s03">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="订舱编号" name="s16">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="业务来源" name="s04">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="装运方式" name="s05">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="结算方式" name="s06">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="会计期间" name="s07">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="揽货人" name="s09">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="操作" name="s10">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="订舱单证" name="s11">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="订舱客服" name="s12">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="销售助理" name="s13">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="航线客服" name="s14">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="海外客服" name="s15">
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Divider style={{ margin: '10px 0' }} />
      <Row>
        <Col span={8}>
          <Row>
            <Col span={15}>
              <Form.Item labelCol={{ flex: '100px' }} label="委托单位" name="customer" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
            </Col>
            <Col span={9}>
              <Row>
                <Col flex='auto'>
                  <Form.Item labelCol={{ flex: '60px' }} label="收发通" name="scn">
                    <Select />
                  </Form.Item>
                </Col>
                <Col flex='20px'>
                  <Button icon={<SaveOutlined style={{ color: '#ddd' }} />} />
                </Col>
              </Row>
            </Col>
            <Col span={24}>
              <Form.Item name="shipper">
                <Shipper label="Shipper(发货人)" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item name="consignee">
                <Shipper label="Consignee(收货人)" />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item name="notify">
                <Shipper label="Notify Party(通知人)" />
              </Form.Item>
            </Col>
          </Row>
        </Col>
        <Col span={16}>
          <Row>
            <Col span={8}>
              <Form.Item labelCol={{ flex: '100px' }} label="客户操作" name="s01">
                <Input />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item labelCol={{ flex: '100px' }} label="联系方式" name="s02">
                <Input />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item labelCol={{ flex: '100px' }} label="服务合同号" name="s03">
                <Input.Password />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item labelCol={{ flex: '100px' }} label="船代" name="s21">
                <Select />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item labelCol={{ flex: '100px' }} label="截单时间" name="dps22">
                <DatePicker
                  showTime={{
                    format: 'HH:mm',
                  }}
                  format="YYYY-MM-DD HH:mm"
                  style={{width:'100%'}}
                />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item labelCol={{ flex: '100px' }} label="截港时间" name="s23">
                <DatePicker showTime style={{width:'100%'}}/>
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item labelCol={{ flex: '100px' }} label="场站" name="s04">
                <Select />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item labelCol={{ flex: '100px' }} label="场站联系人" name="s05">
                <Input />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item labelCol={{ flex: '100px' }} label="场站电话" name="s06">
                <Input />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item labelCol={{ flex: '100px' }} label="海外代理" name="s07">
                <Select />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item labelCol={{ flex: '100px' }} label="订舱代理" name="s08">
                <Select />
              </Form.Item>
            </Col>

            <Col span={16}>
              <Form.Item labelCol={{ flex: '100px' }} label="AGENT" name="s10">
                <Input.TextArea style={{ height: '56px' }} />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Row>
                <Col span={24}>
                  <Form.Item labelCol={{ flex: '100px' }} label="交货日期" name="s11">
                    <Select />
                  </Form.Item>
                </Col>
                <Col span={24}>
                  <Form.Item labelCol={{ flex: '100px' }} label="仓库" name="s12">
                    <Select />
                  </Form.Item>
                </Col>
              </Row>
            </Col>
            <Col span={24} style={{ paddingLeft: '20px' }}>
              <Form.Item label="集装箱信息"></Form.Item>
            </Col>
            <Col span={24} style={{ paddingLeft: '20px' }}>
              <Table
                size='small'
                columns={columns}
                dataSource={dataSource}
                scroll={{ x: 1600, y: 235 }}
                pagination={false}
                bordered
              />
            </Col>
          </Row>
        </Col>
      </Row>
      <Row>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="ETD" name="m01" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="ATD" name="m02">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="船名" name="m03" rules={[{ required: true }]}>
            <Select
              dropdownRender={(menu) => (
                <>
                  {menu}
                  <Divider
                    style={{
                      margin: '8px 0',
                    }}
                  />
                  <Space>
                    <Form
                      form={formRef}
                      onFinish={addItem}
                      style={{ margin: '0 10px -20px 10px' }}
                    ><Row>
                        <Col span={18}>
                          <Form.Item name="newVessel" >
                            <Input placeholder="请输入船名" />
                          </Form.Item>
                        </Col>
                        <Col span={6}>
                          <Form.Item>
                            <Button type="primary" htmlType="submit">
                              新建
                            </Button>
                          </Form.Item>
                        </Col>
                      </Row>
                    </Form>
                  </Space>
                </>
              )}
              options={items.map((item) => ({
                label: item,
                value: item,
              }))}
            />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="航次" name="m04">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="船公司" name="m04">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="所属航线" name="m05">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="收货地" name="m051">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item label="" name="m052">
            <CopyInput size="small" />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="装货港" name="m053">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item label="" name="m054">
            <CopyInput size="small" />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="卸货港" name="m055">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item label="" name="m056">
            <CopyInput size="small" />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="交货地" name="m057">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item label="" name="m058">
            <CopyInput size="small" />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="中转港" name="m059">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item label="" name="m05a">
            <CopyInput size="small" />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="预抵日期" name="m06">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="货物标示" name="tags">
            <Select onChange={changeTag}>
              <Option value="S">普通货</Option>
              <Option value="D">危险品</Option>
              <Option value="R">冻 柜</Option>
              <Option value="O">超限箱</Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
      {isDanger &&
        <Row>
          <Col lg={8} xl={6} xxl={4}>
            <Form.Item labelCol={{ flex: '100px' }} label="危险品等级" name="D01">
              <Input />
            </Form.Item>
          </Col>
          <Col lg={8} xl={6} xxl={4}>
            <Form.Item labelCol={{ flex: '100px' }} label="危险品编号" name="D02">
              <Input />
            </Form.Item>
          </Col>
          <Col lg={8} xl={6} xxl={4}>
            <Form.Item labelCol={{ flex: '100px' }} label="危险品页号" name="D03">
              <Input />
            </Form.Item>
          </Col>
          <Col lg={8} xl={6} xxl={4}>
            <Form.Item labelCol={{ flex: '100px' }} label="危险品标签" name="D04">
              <Input />
            </Form.Item>
          </Col>
          <Col lg={8} xl={6} xxl={4}>
            <Form.Item labelCol={{ flex: '100px' }} label="危险品联系人" name="D04">
              <Input />
            </Form.Item>
          </Col>
        </Row>
      }
      {isReefer &&
        <Row>
          <Col lg={8} xl={6} xxl={4}>
            <Form.Item labelCol={{ flex: '100px' }} label="通风量" name="R01">
              <Input />
            </Form.Item>
          </Col>
          <Col lg={8} xl={6} xxl={4}>
            <Form.Item labelCol={{ flex: '100px' }} label="湿度" name="R04">
              <Input />
            </Form.Item>
          </Col>
          <Col lg={8} xl={6} xxl={4}>
            <Form.Item labelCol={{ flex: '100px' }} label="设置温度" name="R02">
              <Input />
            </Form.Item>
          </Col>
          <Col lg={8} xl={6} xxl={4}>
            <Form.Item labelCol={{ flex: '100px' }} label="温度单位" name="R03">
              <Select />
            </Form.Item>
          </Col>
          <Col lg={8} xl={6} xxl={4}>
            <Form.Item labelCol={{ flex: '100px' }} label="最低温度" name="R04">
              <Input />
            </Form.Item>
          </Col>
          <Col lg={8} xl={6} xxl={4}>
            <Form.Item labelCol={{ flex: '100px' }} label="最高温度" name="R04">
              <Input />
            </Form.Item>
          </Col>
        </Row>
      }
      <Row gutter={8}>
        <Col span={5}>
          <Form.Item label="Marks & Numbers（唛头）" colon={false}></Form.Item>
          <Form.Item name='cargoMarks' >
            <Input.TextArea rows={8} style={{ resize: 'none' }} />
          </Form.Item>
        </Col>
        <Col span={9}>
          {/* <Form.Item label="Description of Goods（货物描述）" colon={false}></Form.Item>
          <Form.Item name='cargoDescription'>
            <Input.TextArea rows={8} style={{ resize: 'none' }} />
          </Form.Item> */}
          <Form.Item name="cargoDescription">
            <Shipper label="Description of Goods（货物描述）" height={178} />
          </Form.Item>
        </Col>
        <Col span={6}>
          <Row>
            <Col span={24}>
              <Form.Item label="No of Pkgs or Units（件数/包装类型）" colon={false}></Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item name='cargoPkgsUnits' style={{ marginBottom: '-1px' }}>
                <Input.TextArea rows={7} style={{ resize: 'none' }} readOnly />
              </Form.Item>
            </Col>
            <Col span={14}>
              <Form.Item name='cargoPkgsUnits'>
                <Input addonBefore="件数/包装" />
              </Form.Item>
            </Col>
            <Col span={10}>
              <Form.Item name='cargoPkgsUnits'>
                <Select />
              </Form.Item>
            </Col>
          </Row>
        </Col>
        <Col span={4}>
          <Row>
            <Col span={24}>
              <Form.Item label="Gross Weight（毛重KGS）" colon={false}></Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item name='cargoWeight' style={{ marginBottom: '-1px' }}>
                <Input.TextArea rows={2} style={{ resize: 'none' }} />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item name='cargoPkgsUnits'>
                <Input suffix="KGS" addonBefore="重量" type="number" />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={24}>
              <Form.Item label="Measurement（尺码CBM）" colon={false}></Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item name='cargoSize' style={{ marginBottom: '-1px' }}>
                <Input.TextArea rows={2} style={{ resize: 'none' }} />
              </Form.Item>
            </Col>
            <Col span={24}>
              <Form.Item name='cargoPkgsUnits'>
                <Input suffix="CBM" addonBefore="尺码" />
              </Form.Item>
            </Col>
          </Row>
        </Col>
      </Row>
      <Row>
        <Col lg={8} xl={12} xxl={16}>
          <Form.Item name='cargoPkgsUnits' label='件数大写' labelCol={{ flex: '100px' }}>
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item name='time' label='签单时间' labelCol={{ flex: '100px' }}>
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item name='place' label='签单地点' labelCol={{ flex: '100px' }}>
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="运输条款" name="x01">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="付费方式" name="x02">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="签单方式" name="x03">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="正本份数" name="x04">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="副本份数" name="x05">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="是否改签" name="x06">
            <Switch />
          </Form.Item>
        </Col>
      </Row>
      <Divider style={{ margin: '10px 0' }} />
      <Row>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="承运车队" name="t01">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="联 系 人" name="t03">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="联系方式" name="t06">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="派车时间" name="t04">
            <Select />
          </Form.Item>
        </Col>
        <Col lg={8} xl={12} xxl={8}>
          <Form.Item labelCol={{ flex: '100px' }} label="提货地址" name="t04">
            <Input />
          </Form.Item>
        </Col>
      </Row>
      <Divider style={{ margin: '10px 0' }} />
      <Row>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="报 关 行" name="t01">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="报关单号" name="t03">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="贸易方式" name="t06">
            <Input />
          </Form.Item>
        </Col>
        <Col lg={8} xl={6} xxl={4}>
          <Form.Item labelCol={{ flex: '100px' }} label="贸易条款" name="t04">
            <Select />
          </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col span={14}>
          <Form.Item labelCol={{ flex: '100px' }} label="操作备注" name="n01">
            <Input.TextArea rows={4} style={{ resize: 'none' }} />
          </Form.Item>
        </Col>
        <Col span={10}>
          <Form.Item labelCol={{ flex: '100px' }} label="订舱备注" name="n02">
            <Input.TextArea rows={4} style={{ resize: 'none' }} />
          </Form.Item>
        </Col>
      </Row>
      {/* <Row>
        <Col span={24}>
          <Space>
            <Button type="primary" htmlType="submit" size='large'>
              保存
            </Button>
            <Button htmlType="button" size='large'>
              重置
            </Button>
          </Space>
        </Col>
      </Row> */}
    </Form>
  </Card>);
}
