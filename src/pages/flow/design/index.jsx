import React, { useRef, useEffect } from 'react';
/** 交互组件 */
import {
    /** XFlow核心组件 */
    XFlow,
    /** 流程图画布组件 */
    FlowchartCanvas,
    /** 流程图配置扩展 */
    FlowchartExtension,
    /** 流程图节点组件 */
    FlowchartNodePanel,
    /** 流程图表单组件 */
    FlowchartFormPanel,
    /** 通用组件：快捷键 */
    KeyBindings,
    /** 通用组件：画布缩放 */
    CanvasScaleToolbar,
    /** 通用组件：右键菜单 */
    CanvasContextMenu,
    /** 通用组件：工具栏 */
    CanvasToolbar,
    /** 通用组件：对齐线 */
    CanvasSnapline,
    /** 通用组件：节点连接桩 */
    CanvasNodePortTooltip,
} from '@antv/xflow';
import '@antv/xflow/dist/index.css';
import { useMenuConfig } from './config/menu';
import { useToolbarConfig } from './config/toolbar';
import { DndNode } from './node';
import { XFlowGraphCommands } from '@antv/xflow';
import { getGraphData } from './mock';

import './index.less';

export default props => {
    const { meta } = props;
    const menucConfig = useMenuConfig(props);
    const toolbarConfig = useToolbarConfig(props);

    const graphRef = useRef();
    const onLoad = async (app) => {
        // console.log(app);
        graphRef.current = await app.getGraphInstance();
        /** Mock从服务端获取数据 */
        const graphData = await getGraphData();

        await app.executeCommand(XFlowGraphCommands.GRAPH_RENDER.id, {
            graphData,
          });

    };
    // useEffect(() => {
    //     if (graphRef.current) {
    //         graphRef.current.on('node:click', (...arg) => {
    //             console.log("Data");
    //         });
    //     }
    // }, [graphRef]);

    const { offsetHeight } = window.document.getElementsByClassName("cala-body")[0]; //获取容器高度

    return (<XFlow className="flow-user-custom-clz" onLoad={onLoad} meta={meta} style={{ height: offsetHeight - 22 }}>
        <CanvasToolbar // 顶部工具栏
            className="xflow-workspace-toolbar-top"
            layout="horizontal"
            position={{ top: 0, left: 0, right: 0, bottom: 0 }}
            config={toolbarConfig}
        />
        <FlowchartExtension />
        <FlowchartNodePanel //左侧元素栏
            show={true}
            showHeader={false}
            showOfficial={false}
            defaultActiveKey={['custom']}
            position={{ width: 200, top: 40, bottom: 0, left: 0 }}
            registerNode={{
                key: 'custom',
                title: '流程节点',
                nodes: [
                    {
                        component: DndNode,
                        name: 'custom-start',
                        width: 60,
                        height: 60,
                        label: '开始',
                    },
                    {
                        component: DndNode,
                        name: 'custom-end',
                        width: 60,
                        height: 60,
                        label: '结束',
                    },
                    {
                        component: DndNode,
                        popover: () => <div>判定</div>,
                        name: 'custom-condition',
                        width: 60,
                        height: 60,
                        label: '判定',
                    },
                    {
                        component: DndNode,
                        popover: () => <div>动作</div>,
                        name: 'custom-action',
                        width: 60,
                        height: 60,
                        label: '动作',
                    },
                    {
                        component: DndNode,
                        popover: () => <div>分支</div>,
                        name: 'custom-branch',
                        width: 60,
                        height: 60,
                        label: '分支',
                    },
                    {
                        component: DndNode,
                        popover: () => <div>聚合</div>,
                        name: 'custom-merge',
                        width: 60,
                        height: 60,
                        label: '聚合',
                    },
                ],
            }}
        />

        <FlowchartCanvas //主画布
            position={{ top: 40, left: 0, right: 0, bottom: 0 }}
        >
            <CanvasScaleToolbar
                layout="horizontal"
                position={{ top: -40, right: 0 }}
                style={{
                    width: 150,
                    left: 'auto',
                    height: 39,
                }}
            />
            <CanvasContextMenu //右键菜单
                config={menucConfig}
            />
            <CanvasSnapline color="#faad14" />
            <CanvasNodePortTooltip />
        </FlowchartCanvas>

        <FlowchartFormPanel //右侧表单组件
            show={true}
            position={{ width: 200, top: 40, bottom: 0, right: 0 }}
        />
        <KeyBindings />
    </XFlow>);
};
