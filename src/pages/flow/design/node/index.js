import React from 'react';
import './index.less';

export const DndNode = props => {
    const { label, stroke, fill, fontFill, fontSize, width, height, name } = props.data;
    return (
        <div className={"node-container"}
            style={{
                width,
                height,
                fontSize,
            }}
        >
            <div
                className={"custom-node " + name}
                style={{
                    borderColor: stroke,
                    backgroundColor: fill,
                    color: fontFill,
                }}
            >
                <span>{label}</span>
            </div>
        </div>
    );
};