import { createCtxMenuConfig } from '@antv/xflow';
import { MenuItemType } from '@antv/xflow';
import { IconStore, XFlowNodeCommands, XFlowEdgeCommands } from '@antv/xflow';
import { DeleteOutlined, EditOutlined, StopOutlined } from '@ant-design/icons';

export const useMenuConfig = createCtxMenuConfig(config => {
    config.setMenuModelService(async (data, model, modelService, toDispose) => {
        const { type, cell } = data;
        console.log(type);
        switch (type) {
            case 'node':
                model.setValue({
                    id: 'root',
                    type: MenuItemType.Root,
                    submenu: [NsCustomMenuItems.DELETE_NODE],
                });
                break;
            case 'edge':
                model.setValue({
                    id: 'root',
                    type: MenuItemType.Root,
                    submenu: [NsCustomMenuItems.DELETE_EDGE],
                });
                break;
            case 'blank':
                model.setValue({
                    id: 'root',
                    type: MenuItemType.Root,
                    submenu: [NsCustomMenuItems.EMPTY_MENU],
                });
                break;
            default:
                model.setValue({
                    id: 'root',
                    type: MenuItemType.Root,
                    submenu: [NsCustomMenuItems.EMPTY_MENU],
                });
                break;
        }
    });
});
/** menuitem 配置 */
export var NsCustomMenuItems;
(function (NsCustomMenuItems) {
    /** 注册菜单依赖的icon */
    IconStore.set('DeleteOutlined', DeleteOutlined);
    IconStore.set('EditOutlined', EditOutlined);
    IconStore.set('StopOutlined', StopOutlined);
    NsCustomMenuItems.DELETE_EDGE = {
        id: XFlowEdgeCommands.DEL_EDGE.id,
        label: '删除边',
        hotkey: 'Delete',
        iconName: 'DeleteOutlined',
        onClick: async ({ target, commandService }) => {
            commandService.executeCommand(XFlowEdgeCommands.DEL_EDGE.id, {
                edgeConfig: target.data,
            });
        },
    };
    NsCustomMenuItems.DELETE_NODE = {
        id: XFlowNodeCommands.DEL_NODE.id,
        label: '删除节点',
        iconName: 'DeleteOutlined',
        hotkey: 'Delete',
        onClick: async ({ target, commandService }) => {
            commandService.executeCommand(XFlowNodeCommands.DEL_NODE.id, {
                nodeConfig: { id: target.data.id },
            });
        },
    };
    NsCustomMenuItems.EMPTY_MENU = {
        id: 'EMPTY_MENU_ITEM',
        label: '暂无可用',
        isEnabled: false,
        iconName: 'DeleteOutlined',
        onClick: async ({ target, commandService }) => {
            commandService.executeCommand(XFlowNodeCommands.DEL_NODE.id, {
                nodeConfig: { id: target.data.id },
            });
        },
    };
    NsCustomMenuItems.SEPARATOR = {
        id: 'separator',
        type: MenuItemType.Separator,
    };
})(NsCustomMenuItems || (NsCustomMenuItems = {}));
