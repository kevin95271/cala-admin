import { createToolbarConfig } from '@antv/xflow';
import { XFlowGraphCommands, XFlowNodeCommands, IconStore } from '@antv/xflow';
import { SaveOutlined, PlusCircleOutlined, DeleteOutlined } from '@ant-design/icons';
import { message } from 'antd';
var NsConfig;
(function (NsConfig) {
    /** 注册icon 类型 */
    IconStore.set('PlusCircleOutlined', PlusCircleOutlined);
    IconStore.set('DeleteOutlined', DeleteOutlined);
    IconStore.set('SaveOutlined', SaveOutlined);

    /** 获取toobar配置项 */
    NsConfig.getToolbarItems = async () => {
        const toolbarGroup1 = [];
        
        /** 保存数据 */
        toolbarGroup1.push({
            id: XFlowGraphCommands.SAVE_GRAPH_DATA.id,
            iconName: 'SaveOutlined',
            tooltip: '保存数据',
            onClick: async ({ commandService }) => {
                commandService.executeCommand(XFlowGraphCommands.SAVE_GRAPH_DATA.id, {
                    saveGraphDataService: async (meta, data) => {
                        console.log(meta, data);
                        message.success('保存成功！');
                    },
                });
            },
        });

        /** 清空数据 */
        toolbarGroup1.push({
            id: XFlowGraphCommands.SAVE_GRAPH_DATA.id,
            iconName: 'DeleteOutlined',
            tooltip: '清空数据',
            onClick: async ({ commandService }) => {
                console.log(commandService)
            },
        });

        return [
            { name: 'nodeGroup', items: toolbarGroup1 },
        ];
    };
})(NsConfig || (NsConfig = {}));

/** wrap出一个hook */
export const useToolbarConfig = createToolbarConfig(toolbarConfig => {
    /** 生产 toolbar item */
    toolbarConfig.setToolbarModelService(async (toolbarModel) => {
        const toolbarItems = await NsConfig.getToolbarItems();
        toolbarModel.setValue(toolbar => {
            toolbar.mainGroups = toolbarItems;
        });
    });
});
