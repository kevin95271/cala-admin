
// mock data
export const getGraphData = () => {

    const data = {
        "nodes": [{
                "id": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12",
                "renderKey": "custom-start",
                "name": "custom-start",
                "label": "开始",
                "width": 60,
                "height": 60,
                "ports": {
                    "items": [{
                            "group": "top",
                            "id": "a4488b83-715d-4fc0-a7e7-ee06ceb13d5d"
                        },
                        {
                            "group": "right",
                            "id": "c73e7d09-f693-4b16-92a0-5f2a810059e5"
                        },
                        {
                            "group": "bottom",
                            "id": "4caac0e5-2256-49da-8215-cd64b56dd742"
                        },
                        {
                            "group": "left",
                            "id": "9ad55635-ba6c-400d-91eb-e77721ef6ac0"
                        }
                    ],
                    "groups": {
                        "top": {
                            "position": {
                                "name": "top"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "right": {
                            "position": {
                                "name": "right"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "bottom": {
                            "position": {
                                "name": "bottom"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "left": {
                            "position": {
                                "name": "left"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        }
                    }
                },
                "originData": {
                    "name": "custom-start",
                    "width": 60,
                    "height": 60,
                    "label": "开始",
                    "parentKey": "custom"
                },
                "isCustom": true,
                "parentKey": "custom",
                "x": 480,
                "y": 140,
                "zIndex": 10,
                "incomingEdges": null,
                "outgoingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        }
                    },
                    "id": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12:4caac0e5-2256-49da-8215-cd64b56dd742-node-0a1971fa-5f9d-4895-8931-b81e0199ba1c:feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                    "targetPortId": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                    "sourcePortId": "4caac0e5-2256-49da-8215-cd64b56dd742",
                    "zIndex": 1,
                    "data": {
                        "id": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12:4caac0e5-2256-49da-8215-cd64b56dd742-node-0a1971fa-5f9d-4895-8931-b81e0199ba1c:feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                        "targetPortId": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                        "sourcePortId": "4caac0e5-2256-49da-8215-cd64b56dd742",
                        "source": {
                            "cell": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12",
                            "port": "4caac0e5-2256-49da-8215-cd64b56dd742"
                        },
                        "target": {
                            "cell": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                            "port": "feb162f5-7313-4d55-a71a-fdcfa9dd914f"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                            "sourcePortId": "4caac0e5-2256-49da-8215-cd64b56dd742",
                            "source": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12",
                            "target": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "4caac0e5-2256-49da-8215-cd64b56dd742",
                        "targetPort": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                        "strokeDasharray": [
                            0,
                            0
                        ]
                    },
                    "source": {
                        "cell": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12",
                        "port": "4caac0e5-2256-49da-8215-cd64b56dd742"
                    },
                    "target": {
                        "cell": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                        "port": "feb162f5-7313-4d55-a71a-fdcfa9dd914f"
                    },
                    "labels": [{
                        "targetPortId": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                        "sourcePortId": "4caac0e5-2256-49da-8215-cd64b56dd742",
                        "source": {
                            "cell": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12",
                            "port": "4caac0e5-2256-49da-8215-cd64b56dd742"
                        },
                        "target": {
                            "cell": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                            "port": "feb162f5-7313-4d55-a71a-fdcfa9dd914f"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                            "sourcePortId": "4caac0e5-2256-49da-8215-cd64b56dd742",
                            "source": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12",
                            "target": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12:4caac0e5-2256-49da-8215-cd64b56dd742-node-0a1971fa-5f9d-4895-8931-b81e0199ba1c:feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                        "sourcePort": "4caac0e5-2256-49da-8215-cd64b56dd742",
                        "targetPort": "feb162f5-7313-4d55-a71a-fdcfa9dd914f"
                    }]
                }]
            },
            {
                "id": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                "renderKey": "custom-condition",
                "name": "custom-condition",
                "label": "判定",
                "width": 60,
                "height": 60,
                "ports": {
                    "items": [{
                            "group": "top",
                            "id": "3d998928-2420-44df-a015-f6087c6adf45"
                        },
                        {
                            "group": "right",
                            "id": "286afa15-cadb-467a-a9a7-26711cd3e7cd"
                        },
                        {
                            "group": "bottom",
                            "id": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9"
                        },
                        {
                            "group": "left",
                            "id": "b2a65e31-6fa6-439e-b643-7df2cf243bfe"
                        }
                    ],
                    "groups": {
                        "top": {
                            "position": {
                                "name": "top"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "right": {
                            "position": {
                                "name": "right"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "bottom": {
                            "position": {
                                "name": "bottom"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "left": {
                            "position": {
                                "name": "left"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        }
                    }
                },
                "originData": {
                    "name": "custom-condition",
                    "width": 60,
                    "height": 60,
                    "label": "判定",
                    "parentKey": "custom"
                },
                "isCustom": true,
                "parentKey": "custom",
                "x": 480,
                "y": 362,
                "zIndex": 10,
                "incomingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": [
                                0,
                                0
                            ],
                            "label": "提交"
                        }
                    },
                    "id": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c:326fe1d8-257d-4caf-99d5-a7b30076a75f-node-4a9b66a4-334d-4870-9705-5e91d4c95486:3d998928-2420-44df-a015-f6087c6adf45",
                    "targetPortId": "3d998928-2420-44df-a015-f6087c6adf45",
                    "sourcePortId": "326fe1d8-257d-4caf-99d5-a7b30076a75f",
                    "zIndex": 1,
                    "data": {
                        "id": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c:326fe1d8-257d-4caf-99d5-a7b30076a75f-node-4a9b66a4-334d-4870-9705-5e91d4c95486:3d998928-2420-44df-a015-f6087c6adf45",
                        "targetPortId": "3d998928-2420-44df-a015-f6087c6adf45",
                        "sourcePortId": "326fe1d8-257d-4caf-99d5-a7b30076a75f",
                        "source": {
                            "cell": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                            "port": "326fe1d8-257d-4caf-99d5-a7b30076a75f"
                        },
                        "target": {
                            "cell": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                            "port": "3d998928-2420-44df-a015-f6087c6adf45"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1,
                                "label": "提交"
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "3d998928-2420-44df-a015-f6087c6adf45",
                            "sourcePortId": "326fe1d8-257d-4caf-99d5-a7b30076a75f",
                            "source": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                            "target": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "326fe1d8-257d-4caf-99d5-a7b30076a75f",
                        "targetPort": "3d998928-2420-44df-a015-f6087c6adf45",
                        "strokeDasharray": [
                            0,
                            0
                        ],
                        "label": "提交"
                    },
                    "source": {
                        "cell": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                        "port": "326fe1d8-257d-4caf-99d5-a7b30076a75f"
                    },
                    "target": {
                        "cell": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                        "port": "3d998928-2420-44df-a015-f6087c6adf45"
                    },
                    "labels": [{
                        "attrs": {
                            "label": {
                                "text": "提交"
                            }
                        }
                    }]
                }],
                "outgoingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        }
                    },
                    "id": "node-4a9b66a4-334d-4870-9705-5e91d4c95486:afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9-node-90845bad-2898-402b-afec-3811eafa5689:54ef7109-d40b-4094-8f23-c415ef483617",
                    "targetPortId": "54ef7109-d40b-4094-8f23-c415ef483617",
                    "sourcePortId": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                    "zIndex": 1,
                    "data": {
                        "id": "node-4a9b66a4-334d-4870-9705-5e91d4c95486:afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9-node-90845bad-2898-402b-afec-3811eafa5689:54ef7109-d40b-4094-8f23-c415ef483617",
                        "targetPortId": "54ef7109-d40b-4094-8f23-c415ef483617",
                        "sourcePortId": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                        "source": {
                            "cell": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                            "port": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9"
                        },
                        "target": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "54ef7109-d40b-4094-8f23-c415ef483617"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "54ef7109-d40b-4094-8f23-c415ef483617",
                            "sourcePortId": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                            "source": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                            "target": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                        "targetPort": "54ef7109-d40b-4094-8f23-c415ef483617",
                        "strokeDasharray": [
                            0,
                            0
                        ]
                    },
                    "strokeDasharray": [
                        0,
                        0
                    ],
                    "source": {
                        "cell": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                        "port": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9"
                    },
                    "target": {
                        "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                        "port": "54ef7109-d40b-4094-8f23-c415ef483617"
                    },
                    "labels": [{
                        "id": "node-4a9b66a4-334d-4870-9705-5e91d4c95486:afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9-node-90845bad-2898-402b-afec-3811eafa5689:54ef7109-d40b-4094-8f23-c415ef483617",
                        "targetPortId": "54ef7109-d40b-4094-8f23-c415ef483617",
                        "sourcePortId": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                        "source": {
                            "cell": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                            "port": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9"
                        },
                        "target": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "54ef7109-d40b-4094-8f23-c415ef483617"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "54ef7109-d40b-4094-8f23-c415ef483617",
                            "sourcePortId": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                            "source": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                            "target": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                        "targetPort": "54ef7109-d40b-4094-8f23-c415ef483617",
                        "strokeDasharray": [
                            0,
                            0
                        ]
                    }]
                }]
            },
            {
                "id": "node-90845bad-2898-402b-afec-3811eafa5689",
                "renderKey": "custom-branch",
                "name": "custom-branch",
                "label": "分支",
                "width": 50,
                "height": 50,
                "ports": {
                    "items": [{
                            "group": "top",
                            "id": "54ef7109-d40b-4094-8f23-c415ef483617"
                        },
                        {
                            "group": "right",
                            "id": "14b73640-8e64-49d0-b475-d689ed714529"
                        },
                        {
                            "group": "bottom",
                            "id": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        {
                            "group": "left",
                            "id": "d04e6e50-abe2-4ca3-b0f3-df59572875c9"
                        }
                    ],
                    "groups": {
                        "top": {
                            "position": {
                                "name": "top"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "right": {
                            "position": {
                                "name": "right"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "bottom": {
                            "position": {
                                "name": "bottom"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "left": {
                            "position": {
                                "name": "left"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        }
                    }
                },
                "originData": {
                    "name": "custom-branch",
                    "width": 50,
                    "height": 50,
                    "label": "分支",
                    "parentKey": "custom"
                },
                "isCustom": true,
                "parentKey": "custom",
                "x": 485,
                "y": 479,
                "zIndex": 10,
                "incomingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        }
                    },
                    "id": "node-4a9b66a4-334d-4870-9705-5e91d4c95486:afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9-node-90845bad-2898-402b-afec-3811eafa5689:54ef7109-d40b-4094-8f23-c415ef483617",
                    "targetPortId": "54ef7109-d40b-4094-8f23-c415ef483617",
                    "sourcePortId": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                    "zIndex": 1,
                    "data": {
                        "id": "node-4a9b66a4-334d-4870-9705-5e91d4c95486:afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9-node-90845bad-2898-402b-afec-3811eafa5689:54ef7109-d40b-4094-8f23-c415ef483617",
                        "targetPortId": "54ef7109-d40b-4094-8f23-c415ef483617",
                        "sourcePortId": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                        "source": {
                            "cell": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                            "port": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9"
                        },
                        "target": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "54ef7109-d40b-4094-8f23-c415ef483617"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "54ef7109-d40b-4094-8f23-c415ef483617",
                            "sourcePortId": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                            "source": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                            "target": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                        "targetPort": "54ef7109-d40b-4094-8f23-c415ef483617",
                        "strokeDasharray": [
                            0,
                            0
                        ]
                    },
                    "strokeDasharray": [
                        0,
                        0
                    ],
                    "source": {
                        "cell": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                        "port": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9"
                    },
                    "target": {
                        "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                        "port": "54ef7109-d40b-4094-8f23-c415ef483617"
                    },
                    "labels": [{
                        "id": "node-4a9b66a4-334d-4870-9705-5e91d4c95486:afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9-node-90845bad-2898-402b-afec-3811eafa5689:54ef7109-d40b-4094-8f23-c415ef483617",
                        "targetPortId": "54ef7109-d40b-4094-8f23-c415ef483617",
                        "sourcePortId": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                        "source": {
                            "cell": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                            "port": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9"
                        },
                        "target": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "54ef7109-d40b-4094-8f23-c415ef483617"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "54ef7109-d40b-4094-8f23-c415ef483617",
                            "sourcePortId": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                            "source": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                            "target": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                        "targetPort": "54ef7109-d40b-4094-8f23-c415ef483617",
                        "strokeDasharray": [
                            0,
                            0
                        ]
                    }]
                }],
                "outgoingEdges": [{
                        "shape": "edge",
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "strokeWidth": 1,
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ]
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-c133a553-2165-4917-9ce6-b201adccab43:37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                        "targetPortId": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "zIndex": 1,
                        "data": {
                            "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-c133a553-2165-4917-9ce6-b201adccab43:37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                            "targetPortId": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": {
                                "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                            },
                            "target": {
                                "cell": "node-c133a553-2165-4917-9ce6-b201adccab43",
                                "port": "37c3f28d-30eb-4f8d-954b-cd9026e3650a"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": [
                                        0,
                                        0
                                    ],
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                                "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "target": "node-c133a553-2165-4917-9ce6-b201adccab43",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "targetPort": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        },
                        "strokeDasharray": [
                            0,
                            0
                        ],
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-c133a553-2165-4917-9ce6-b201adccab43",
                            "port": "37c3f28d-30eb-4f8d-954b-cd9026e3650a"
                        },
                        "labels": [{
                            "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-c133a553-2165-4917-9ce6-b201adccab43:37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                            "targetPortId": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": {
                                "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                            },
                            "target": {
                                "cell": "node-c133a553-2165-4917-9ce6-b201adccab43",
                                "port": "37c3f28d-30eb-4f8d-954b-cd9026e3650a"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": [
                                        0,
                                        0
                                    ],
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                                "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "target": "node-c133a553-2165-4917-9ce6-b201adccab43",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "targetPort": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        }]
                    },
                    {
                        "shape": "edge",
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "strokeWidth": 1,
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ]
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3:85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                        "targetPortId": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "zIndex": 1,
                        "data": {
                            "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3:85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                            "targetPortId": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": {
                                "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                            },
                            "target": {
                                "cell": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                                "port": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": [
                                        0,
                                        0
                                    ],
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                                "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "target": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "targetPort": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        },
                        "strokeDasharray": [
                            0,
                            0
                        ],
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                            "port": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d"
                        },
                        "labels": [{
                            "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3:85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                            "targetPortId": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": {
                                "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                            },
                            "target": {
                                "cell": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                                "port": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": [
                                        0,
                                        0
                                    ],
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                                "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "target": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "targetPort": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        }]
                    },
                    {
                        "shape": "edge",
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "strokeWidth": 1,
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5"
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-80ff002d-4759-4d66-9d0b-de645012d1e6:80141f60-f2f8-4e62-b228-bef6371003dc",
                        "targetPortId": "80141f60-f2f8-4e62-b228-bef6371003dc",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "80141f60-f2f8-4e62-b228-bef6371003dc",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": {
                                "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                            },
                            "target": {
                                "cell": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                                "port": "80141f60-f2f8-4e62-b228-bef6371003dc"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "80141f60-f2f8-4e62-b228-bef6371003dc",
                                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                                "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "target": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-80ff002d-4759-4d66-9d0b-de645012d1e6:80141f60-f2f8-4e62-b228-bef6371003dc",
                            "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "targetPort": "80141f60-f2f8-4e62-b228-bef6371003dc"
                        },
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                            "port": "80141f60-f2f8-4e62-b228-bef6371003dc"
                        },
                        "labels": [{
                            "targetPortId": "80141f60-f2f8-4e62-b228-bef6371003dc",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": {
                                "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                            },
                            "target": {
                                "cell": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                                "port": "80141f60-f2f8-4e62-b228-bef6371003dc"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "80141f60-f2f8-4e62-b228-bef6371003dc",
                                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                                "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "target": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-80ff002d-4759-4d66-9d0b-de645012d1e6:80141f60-f2f8-4e62-b228-bef6371003dc",
                            "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "targetPort": "80141f60-f2f8-4e62-b228-bef6371003dc"
                        }]
                    },
                    {
                        "shape": "edge",
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "strokeWidth": 1,
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5"
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-1c17c04c-9283-42a4-aed4-fac6bdde195f:98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                        "targetPortId": "98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": {
                                "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                            },
                            "target": {
                                "cell": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                                "port": "98662d47-3808-4e49-b3d7-e99f38a0b7d3"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                                "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "target": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-1c17c04c-9283-42a4-aed4-fac6bdde195f:98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                            "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "targetPort": "98662d47-3808-4e49-b3d7-e99f38a0b7d3"
                        },
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                            "port": "98662d47-3808-4e49-b3d7-e99f38a0b7d3"
                        },
                        "labels": [{
                            "targetPortId": "98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": {
                                "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                            },
                            "target": {
                                "cell": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                                "port": "98662d47-3808-4e49-b3d7-e99f38a0b7d3"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                                "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "target": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-1c17c04c-9283-42a4-aed4-fac6bdde195f:98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                            "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "targetPort": "98662d47-3808-4e49-b3d7-e99f38a0b7d3"
                        }]
                    },
                    {
                        "shape": "edge",
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "strokeWidth": 1,
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5"
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd:b622ae94-b250-40ac-b003-54ff27e104bd",
                        "targetPortId": "b622ae94-b250-40ac-b003-54ff27e104bd",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "b622ae94-b250-40ac-b003-54ff27e104bd",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": {
                                "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                            },
                            "target": {
                                "cell": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                                "port": "b622ae94-b250-40ac-b003-54ff27e104bd"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "b622ae94-b250-40ac-b003-54ff27e104bd",
                                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                                "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "target": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd:b622ae94-b250-40ac-b003-54ff27e104bd",
                            "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "targetPort": "b622ae94-b250-40ac-b003-54ff27e104bd"
                        },
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                            "port": "b622ae94-b250-40ac-b003-54ff27e104bd"
                        },
                        "labels": [{
                            "targetPortId": "b622ae94-b250-40ac-b003-54ff27e104bd",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": {
                                "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                            },
                            "target": {
                                "cell": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                                "port": "b622ae94-b250-40ac-b003-54ff27e104bd"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "b622ae94-b250-40ac-b003-54ff27e104bd",
                                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                                "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "target": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd:b622ae94-b250-40ac-b003-54ff27e104bd",
                            "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "targetPort": "b622ae94-b250-40ac-b003-54ff27e104bd"
                        }]
                    },
                    {
                        "shape": "edge",
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "strokeWidth": 1,
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5"
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-f16c4260-b46e-4308-9d8b-428d1c9a42e0:39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                        "targetPortId": "39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": {
                                "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                            },
                            "target": {
                                "cell": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                                "port": "39dd9d6d-267f-4841-bde6-533b40ae5dd0"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                                "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "target": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-f16c4260-b46e-4308-9d8b-428d1c9a42e0:39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                            "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "targetPort": "39dd9d6d-267f-4841-bde6-533b40ae5dd0"
                        },
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                            "port": "39dd9d6d-267f-4841-bde6-533b40ae5dd0"
                        },
                        "labels": [{
                            "targetPortId": "39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": {
                                "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                            },
                            "target": {
                                "cell": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                                "port": "39dd9d6d-267f-4841-bde6-533b40ae5dd0"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                                "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                                "target": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-f16c4260-b46e-4308-9d8b-428d1c9a42e0:39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                            "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "targetPort": "39dd9d6d-267f-4841-bde6-533b40ae5dd0"
                        }]
                    }
                ]
            },
            {
                "id": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                "renderKey": "custom-action",
                "name": "custom-action",
                "label": "行政部",
                "width": 80,
                "height": 50,
                "ports": {
                    "items": [{
                            "group": "top",
                            "id": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d"
                        },
                        {
                            "group": "right",
                            "id": "eb55970c-6f3d-4837-abac-8136d64a56f5"
                        },
                        {
                            "group": "bottom",
                            "id": "a5ff8093-70e2-448f-a0fe-3d69060fd457"
                        },
                        {
                            "group": "left",
                            "id": "7d645d98-bac3-402e-beec-a64fedc967ab"
                        }
                    ],
                    "groups": {
                        "top": {
                            "position": {
                                "name": "top"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "right": {
                            "position": {
                                "name": "right"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "bottom": {
                            "position": {
                                "name": "bottom"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "left": {
                            "position": {
                                "name": "left"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        }
                    }
                },
                "originData": {
                    "name": "custom-action",
                    "width": 80,
                    "height": 50,
                    "label": "动作",
                    "parentKey": "custom"
                },
                "isCustom": true,
                "parentKey": "custom",
                "x": 535,
                "y": 612,
                "zIndex": 10,
                "incomingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        }
                    },
                    "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3:85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                    "targetPortId": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                    "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                    "zIndex": 1,
                    "data": {
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3:85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                        "targetPortId": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                            "port": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "target": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "targetPort": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                        "strokeDasharray": [
                            0,
                            0
                        ]
                    },
                    "source": {
                        "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                        "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                    },
                    "target": {
                        "cell": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                        "port": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d"
                    },
                    "labels": [{
                        "targetPortId": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                            "port": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "target": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3:85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                        "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "targetPort": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d"
                    }]
                }],
                "outgoingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        }
                    },
                    "id": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3:a5ff8093-70e2-448f-a0fe-3d69060fd457-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "sourcePortId": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                    "zIndex": 1,
                    "data": {
                        "id": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3:a5ff8093-70e2-448f-a0fe-3d69060fd457-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                        "source": {
                            "cell": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                            "port": "a5ff8093-70e2-448f-a0fe-3d69060fd457"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                            "source": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                            "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                        "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "strokeDasharray": [
                            0,
                            0
                        ]
                    },
                    "source": {
                        "cell": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                        "port": "a5ff8093-70e2-448f-a0fe-3d69060fd457"
                    },
                    "target": {
                        "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                        "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    },
                    "labels": [{
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                        "source": {
                            "cell": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                            "port": "a5ff8093-70e2-448f-a0fe-3d69060fd457"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                            "source": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                            "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3:a5ff8093-70e2-448f-a0fe-3d69060fd457-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePort": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                        "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    }]
                }],
                "stroke": "#8b572a",
                "fill": "#ffffff",
                "fontFill": "#8b572a"
            },
            {
                "id": "node-c133a553-2165-4917-9ce6-b201adccab43",
                "renderKey": "custom-action",
                "name": "custom-action",
                "label": "业务部",
                "width": 80,
                "height": 50,
                "ports": {
                    "items": [{
                            "group": "top",
                            "id": "37c3f28d-30eb-4f8d-954b-cd9026e3650a"
                        },
                        {
                            "group": "right",
                            "id": "481777d3-ce31-4d2e-a59a-3b5aaacd42d7"
                        },
                        {
                            "group": "bottom",
                            "id": "06071849-6e12-4060-97cc-b214d9f2290a"
                        },
                        {
                            "group": "left",
                            "id": "197832b5-930e-4166-a2a3-666aaccedf34"
                        }
                    ],
                    "groups": {
                        "top": {
                            "position": {
                                "name": "top"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "right": {
                            "position": {
                                "name": "right"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "bottom": {
                            "position": {
                                "name": "bottom"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "left": {
                            "position": {
                                "name": "left"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        }
                    }
                },
                "originData": {
                    "name": "custom-action",
                    "width": 80,
                    "height": 50,
                    "label": "动作",
                    "parentKey": "custom"
                },
                "isCustom": true,
                "parentKey": "custom",
                "x": 400,
                "y": 612,
                "zIndex": 10,
                "incomingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        }
                    },
                    "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-c133a553-2165-4917-9ce6-b201adccab43:37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                    "targetPortId": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                    "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                    "zIndex": 1,
                    "data": {
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-c133a553-2165-4917-9ce6-b201adccab43:37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                        "targetPortId": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-c133a553-2165-4917-9ce6-b201adccab43",
                            "port": "37c3f28d-30eb-4f8d-954b-cd9026e3650a"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "target": "node-c133a553-2165-4917-9ce6-b201adccab43",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "targetPort": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                        "strokeDasharray": [
                            0,
                            0
                        ]
                    },
                    "source": {
                        "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                        "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                    },
                    "target": {
                        "cell": "node-c133a553-2165-4917-9ce6-b201adccab43",
                        "port": "37c3f28d-30eb-4f8d-954b-cd9026e3650a"
                    },
                    "labels": [{
                        "targetPortId": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-c133a553-2165-4917-9ce6-b201adccab43",
                            "port": "37c3f28d-30eb-4f8d-954b-cd9026e3650a"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "target": "node-c133a553-2165-4917-9ce6-b201adccab43",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-c133a553-2165-4917-9ce6-b201adccab43:37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                        "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "targetPort": "37c3f28d-30eb-4f8d-954b-cd9026e3650a"
                    }]
                }],
                "outgoingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        }
                    },
                    "id": "node-c133a553-2165-4917-9ce6-b201adccab43:06071849-6e12-4060-97cc-b214d9f2290a-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "sourcePortId": "06071849-6e12-4060-97cc-b214d9f2290a",
                    "zIndex": 1,
                    "data": {
                        "id": "node-c133a553-2165-4917-9ce6-b201adccab43:06071849-6e12-4060-97cc-b214d9f2290a-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "06071849-6e12-4060-97cc-b214d9f2290a",
                        "source": {
                            "cell": "node-c133a553-2165-4917-9ce6-b201adccab43",
                            "port": "06071849-6e12-4060-97cc-b214d9f2290a"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "06071849-6e12-4060-97cc-b214d9f2290a",
                            "source": "node-c133a553-2165-4917-9ce6-b201adccab43",
                            "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "06071849-6e12-4060-97cc-b214d9f2290a",
                        "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "strokeDasharray": [
                            0,
                            0
                        ]
                    },
                    "source": {
                        "cell": "node-c133a553-2165-4917-9ce6-b201adccab43",
                        "port": "06071849-6e12-4060-97cc-b214d9f2290a"
                    },
                    "target": {
                        "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                        "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    },
                    "labels": [{
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "06071849-6e12-4060-97cc-b214d9f2290a",
                        "source": {
                            "cell": "node-c133a553-2165-4917-9ce6-b201adccab43",
                            "port": "06071849-6e12-4060-97cc-b214d9f2290a"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "06071849-6e12-4060-97cc-b214d9f2290a",
                            "source": "node-c133a553-2165-4917-9ce6-b201adccab43",
                            "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-c133a553-2165-4917-9ce6-b201adccab43:06071849-6e12-4060-97cc-b214d9f2290a-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePort": "06071849-6e12-4060-97cc-b214d9f2290a",
                        "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    }]
                }],
                "stroke": "#f5a623",
                "fontFill": "#f5a623",
                "fill": "#ffffff"
            },
            {
                "id": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                "renderKey": "custom-merge",
                "name": "custom-merge",
                "label": "聚合",
                "width": 50,
                "height": 50,
                "ports": {
                    "items": [{
                            "group": "top",
                            "id": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        {
                            "group": "right",
                            "id": "fd75a5fb-ef98-4ace-be45-1226ecee05ba"
                        },
                        {
                            "group": "bottom",
                            "id": "15399998-fa33-4df2-b3b8-0cdac120b888"
                        },
                        {
                            "group": "left",
                            "id": "f42030f8-02ee-48ea-ae2e-11c2821ceea2"
                        }
                    ],
                    "groups": {
                        "top": {
                            "position": {
                                "name": "top"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "right": {
                            "position": {
                                "name": "right"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "bottom": {
                            "position": {
                                "name": "bottom"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "left": {
                            "position": {
                                "name": "left"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        }
                    }
                },
                "originData": {
                    "name": "custom-merge",
                    "width": 50,
                    "height": 50,
                    "label": "聚合",
                    "parentKey": "custom"
                },
                "isCustom": true,
                "parentKey": "custom",
                "x": 485,
                "y": 706,
                "zIndex": 10,
                "incomingEdges": [{
                        "shape": "edge",
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "strokeWidth": 1,
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ]
                            }
                        },
                        "id": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3:a5ff8093-70e2-448f-a0fe-3d69060fd457-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                        "zIndex": 1,
                        "data": {
                            "id": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3:a5ff8093-70e2-448f-a0fe-3d69060fd457-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                            "source": {
                                "cell": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                                "port": "a5ff8093-70e2-448f-a0fe-3d69060fd457"
                            },
                            "target": {
                                "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": [
                                        0,
                                        0
                                    ],
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                                "sourcePortId": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                                "source": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                                "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "sourcePort": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                            "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        },
                        "strokeDasharray": [
                            0,
                            0
                        ],
                        "source": {
                            "cell": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                            "port": "a5ff8093-70e2-448f-a0fe-3d69060fd457"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "labels": [{
                            "id": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3:a5ff8093-70e2-448f-a0fe-3d69060fd457-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                            "source": {
                                "cell": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                                "port": "a5ff8093-70e2-448f-a0fe-3d69060fd457"
                            },
                            "target": {
                                "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": [
                                        0,
                                        0
                                    ],
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                                "sourcePortId": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                                "source": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                                "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "sourcePort": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                            "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        }]
                    },
                    {
                        "shape": "edge",
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "strokeWidth": 1,
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ]
                            }
                        },
                        "id": "node-c133a553-2165-4917-9ce6-b201adccab43:06071849-6e12-4060-97cc-b214d9f2290a-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "06071849-6e12-4060-97cc-b214d9f2290a",
                        "zIndex": 1,
                        "data": {
                            "id": "node-c133a553-2165-4917-9ce6-b201adccab43:06071849-6e12-4060-97cc-b214d9f2290a-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "06071849-6e12-4060-97cc-b214d9f2290a",
                            "source": {
                                "cell": "node-c133a553-2165-4917-9ce6-b201adccab43",
                                "port": "06071849-6e12-4060-97cc-b214d9f2290a"
                            },
                            "target": {
                                "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": [
                                        0,
                                        0
                                    ],
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                                "sourcePortId": "06071849-6e12-4060-97cc-b214d9f2290a",
                                "source": "node-c133a553-2165-4917-9ce6-b201adccab43",
                                "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "sourcePort": "06071849-6e12-4060-97cc-b214d9f2290a",
                            "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        },
                        "strokeDasharray": [
                            0,
                            0
                        ],
                        "source": {
                            "cell": "node-c133a553-2165-4917-9ce6-b201adccab43",
                            "port": "06071849-6e12-4060-97cc-b214d9f2290a"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "labels": [{
                            "id": "node-c133a553-2165-4917-9ce6-b201adccab43:06071849-6e12-4060-97cc-b214d9f2290a-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "06071849-6e12-4060-97cc-b214d9f2290a",
                            "source": {
                                "cell": "node-c133a553-2165-4917-9ce6-b201adccab43",
                                "port": "06071849-6e12-4060-97cc-b214d9f2290a"
                            },
                            "target": {
                                "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": [
                                        0,
                                        0
                                    ],
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                                "sourcePortId": "06071849-6e12-4060-97cc-b214d9f2290a",
                                "source": "node-c133a553-2165-4917-9ce6-b201adccab43",
                                "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "sourcePort": "06071849-6e12-4060-97cc-b214d9f2290a",
                            "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        }]
                    },
                    {
                        "shape": "edge",
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "strokeWidth": 1,
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5"
                            }
                        },
                        "id": "node-80ff002d-4759-4d66-9d0b-de645012d1e6:316468f3-7c63-439e-a740-4e3ac6bd3079-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                            "source": {
                                "cell": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                                "port": "316468f3-7c63-439e-a740-4e3ac6bd3079"
                            },
                            "target": {
                                "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                                "sourcePortId": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                                "source": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                                "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-80ff002d-4759-4d66-9d0b-de645012d1e6:316468f3-7c63-439e-a740-4e3ac6bd3079-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePort": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                            "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "source": {
                            "cell": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                            "port": "316468f3-7c63-439e-a740-4e3ac6bd3079"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "labels": [{
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                            "source": {
                                "cell": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                                "port": "316468f3-7c63-439e-a740-4e3ac6bd3079"
                            },
                            "target": {
                                "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                                "sourcePortId": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                                "source": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                                "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-80ff002d-4759-4d66-9d0b-de645012d1e6:316468f3-7c63-439e-a740-4e3ac6bd3079-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePort": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                            "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        }]
                    },
                    {
                        "shape": "edge",
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "strokeWidth": 1,
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5"
                            }
                        },
                        "id": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f:6fa9f560-7e0f-4a90-affc-a64ceee400f4-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                            "source": {
                                "cell": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                                "port": "6fa9f560-7e0f-4a90-affc-a64ceee400f4"
                            },
                            "target": {
                                "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                                "sourcePortId": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                                "source": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                                "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f:6fa9f560-7e0f-4a90-affc-a64ceee400f4-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePort": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                            "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "source": {
                            "cell": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                            "port": "6fa9f560-7e0f-4a90-affc-a64ceee400f4"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "labels": [{
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                            "source": {
                                "cell": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                                "port": "6fa9f560-7e0f-4a90-affc-a64ceee400f4"
                            },
                            "target": {
                                "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                                "sourcePortId": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                                "source": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                                "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f:6fa9f560-7e0f-4a90-affc-a64ceee400f4-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePort": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                            "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        }]
                    },
                    {
                        "shape": "edge",
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "strokeWidth": 1,
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5"
                            }
                        },
                        "id": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd:76af2d11-e7c5-44e8-9879-e417d1fd42eb-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                            "source": {
                                "cell": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                                "port": "76af2d11-e7c5-44e8-9879-e417d1fd42eb"
                            },
                            "target": {
                                "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                                "sourcePortId": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                                "source": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                                "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd:76af2d11-e7c5-44e8-9879-e417d1fd42eb-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePort": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                            "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "source": {
                            "cell": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                            "port": "76af2d11-e7c5-44e8-9879-e417d1fd42eb"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "labels": [{
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                            "source": {
                                "cell": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                                "port": "76af2d11-e7c5-44e8-9879-e417d1fd42eb"
                            },
                            "target": {
                                "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                                "sourcePortId": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                                "source": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                                "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd:76af2d11-e7c5-44e8-9879-e417d1fd42eb-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePort": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                            "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        }]
                    },
                    {
                        "shape": "edge",
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "strokeWidth": 1,
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5"
                            }
                        },
                        "id": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0:6aa78a3c-c8f8-4e6c-bf5d-de671c845da5-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                            "source": {
                                "cell": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                                "port": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5"
                            },
                            "target": {
                                "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                                "sourcePortId": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                                "source": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                                "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0:6aa78a3c-c8f8-4e6c-bf5d-de671c845da5-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePort": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                            "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "source": {
                            "cell": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                            "port": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "labels": [{
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                            "source": {
                                "cell": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                                "port": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5"
                            },
                            "target": {
                                "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                            },
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            },
                            "zIndex": 1,
                            "data": {
                                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                                "sourcePortId": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                                "source": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                                "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                                "attrs": {
                                    "line": {
                                        "stroke": "#A2B1C3",
                                        "targetMarker": {
                                            "name": "block",
                                            "width": 12,
                                            "height": 8
                                        },
                                        "strokeDasharray": "5 5",
                                        "strokeWidth": 1
                                    }
                                }
                            },
                            "id": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0:6aa78a3c-c8f8-4e6c-bf5d-de671c845da5-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePort": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                            "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        }]
                    }
                ],
                "outgoingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        }
                    },
                    "id": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:15399998-fa33-4df2-b3b8-0cdac120b888-node-97aba55f-352f-45b4-bf5c-8b195e2e3f74:634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                    "targetPortId": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                    "sourcePortId": "15399998-fa33-4df2-b3b8-0cdac120b888",
                    "zIndex": 1,
                    "data": {
                        "id": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:15399998-fa33-4df2-b3b8-0cdac120b888-node-97aba55f-352f-45b4-bf5c-8b195e2e3f74:634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                        "targetPortId": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                        "sourcePortId": "15399998-fa33-4df2-b3b8-0cdac120b888",
                        "source": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "15399998-fa33-4df2-b3b8-0cdac120b888"
                        },
                        "target": {
                            "cell": "node-97aba55f-352f-45b4-bf5c-8b195e2e3f74",
                            "port": "634b2ee9-537f-4cca-93e9-e8caeb3235aa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                            "sourcePortId": "15399998-fa33-4df2-b3b8-0cdac120b888",
                            "source": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "target": "node-97aba55f-352f-45b4-bf5c-8b195e2e3f74",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "15399998-fa33-4df2-b3b8-0cdac120b888",
                        "targetPort": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                        "strokeDasharray": [
                            0,
                            0
                        ]
                    },
                    "strokeDasharray": [
                        0,
                        0
                    ],
                    "source": {
                        "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                        "port": "15399998-fa33-4df2-b3b8-0cdac120b888"
                    },
                    "target": {
                        "cell": "node-97aba55f-352f-45b4-bf5c-8b195e2e3f74",
                        "port": "634b2ee9-537f-4cca-93e9-e8caeb3235aa"
                    },
                    "labels": [{
                        "id": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:15399998-fa33-4df2-b3b8-0cdac120b888-node-97aba55f-352f-45b4-bf5c-8b195e2e3f74:634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                        "targetPortId": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                        "sourcePortId": "15399998-fa33-4df2-b3b8-0cdac120b888",
                        "source": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "15399998-fa33-4df2-b3b8-0cdac120b888"
                        },
                        "target": {
                            "cell": "node-97aba55f-352f-45b4-bf5c-8b195e2e3f74",
                            "port": "634b2ee9-537f-4cca-93e9-e8caeb3235aa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                            "sourcePortId": "15399998-fa33-4df2-b3b8-0cdac120b888",
                            "source": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "target": "node-97aba55f-352f-45b4-bf5c-8b195e2e3f74",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "15399998-fa33-4df2-b3b8-0cdac120b888",
                        "targetPort": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                        "strokeDasharray": [
                            0,
                            0
                        ]
                    }]
                }]
            },
            {
                "id": "node-97aba55f-352f-45b4-bf5c-8b195e2e3f74",
                "renderKey": "custom-end",
                "name": "custom-end",
                "label": "结束",
                "width": 60,
                "height": 60,
                "ports": {
                    "items": [{
                            "group": "top",
                            "id": "634b2ee9-537f-4cca-93e9-e8caeb3235aa"
                        },
                        {
                            "group": "right",
                            "id": "6e67c520-a0ba-406b-ac9b-46e6e5171fdb"
                        },
                        {
                            "group": "bottom",
                            "id": "f4ed3368-9dd7-4e75-84da-362d91250f85"
                        },
                        {
                            "group": "left",
                            "id": "c254eb9d-91d6-428a-bbac-de0f32f43b58"
                        }
                    ],
                    "groups": {
                        "top": {
                            "position": {
                                "name": "top"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "right": {
                            "position": {
                                "name": "right"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "bottom": {
                            "position": {
                                "name": "bottom"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "left": {
                            "position": {
                                "name": "left"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        }
                    }
                },
                "originData": {
                    "name": "custom-end",
                    "width": 60,
                    "height": 60,
                    "label": "结束",
                    "parentKey": "custom"
                },
                "isCustom": true,
                "parentKey": "custom",
                "x": 480,
                "y": 810,
                "zIndex": 10,
                "incomingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        }
                    },
                    "id": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:15399998-fa33-4df2-b3b8-0cdac120b888-node-97aba55f-352f-45b4-bf5c-8b195e2e3f74:634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                    "targetPortId": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                    "sourcePortId": "15399998-fa33-4df2-b3b8-0cdac120b888",
                    "zIndex": 1,
                    "data": {
                        "id": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:15399998-fa33-4df2-b3b8-0cdac120b888-node-97aba55f-352f-45b4-bf5c-8b195e2e3f74:634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                        "targetPortId": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                        "sourcePortId": "15399998-fa33-4df2-b3b8-0cdac120b888",
                        "source": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "15399998-fa33-4df2-b3b8-0cdac120b888"
                        },
                        "target": {
                            "cell": "node-97aba55f-352f-45b4-bf5c-8b195e2e3f74",
                            "port": "634b2ee9-537f-4cca-93e9-e8caeb3235aa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                            "sourcePortId": "15399998-fa33-4df2-b3b8-0cdac120b888",
                            "source": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "target": "node-97aba55f-352f-45b4-bf5c-8b195e2e3f74",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "15399998-fa33-4df2-b3b8-0cdac120b888",
                        "targetPort": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                        "strokeDasharray": [
                            0,
                            0
                        ]
                    },
                    "source": {
                        "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                        "port": "15399998-fa33-4df2-b3b8-0cdac120b888"
                    },
                    "target": {
                        "cell": "node-97aba55f-352f-45b4-bf5c-8b195e2e3f74",
                        "port": "634b2ee9-537f-4cca-93e9-e8caeb3235aa"
                    },
                    "labels": [{
                        "targetPortId": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                        "sourcePortId": "15399998-fa33-4df2-b3b8-0cdac120b888",
                        "source": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "15399998-fa33-4df2-b3b8-0cdac120b888"
                        },
                        "target": {
                            "cell": "node-97aba55f-352f-45b4-bf5c-8b195e2e3f74",
                            "port": "634b2ee9-537f-4cca-93e9-e8caeb3235aa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                            "sourcePortId": "15399998-fa33-4df2-b3b8-0cdac120b888",
                            "source": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "target": "node-97aba55f-352f-45b4-bf5c-8b195e2e3f74",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:15399998-fa33-4df2-b3b8-0cdac120b888-node-97aba55f-352f-45b4-bf5c-8b195e2e3f74:634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                        "sourcePort": "15399998-fa33-4df2-b3b8-0cdac120b888",
                        "targetPort": "634b2ee9-537f-4cca-93e9-e8caeb3235aa"
                    }]
                }],
                "outgoingEdges": null
            },
            {
                "id": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                "renderKey": "custom-action",
                "name": "custom-action",
                "label": "财务部",
                "width": 80,
                "height": 50,
                "ports": {
                    "items": [{
                            "group": "top",
                            "id": "b622ae94-b250-40ac-b003-54ff27e104bd"
                        },
                        {
                            "group": "right",
                            "id": "7b0c8b7b-5ed1-4a76-b49e-5308b933b63d"
                        },
                        {
                            "group": "bottom",
                            "id": "76af2d11-e7c5-44e8-9879-e417d1fd42eb"
                        },
                        {
                            "group": "left",
                            "id": "4cd7b6ae-cd65-4cb9-a75d-cbe4e61a9ed4"
                        }
                    ],
                    "groups": {
                        "top": {
                            "position": {
                                "name": "top"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "right": {
                            "position": {
                                "name": "right"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "bottom": {
                            "position": {
                                "name": "bottom"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "left": {
                            "position": {
                                "name": "left"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        }
                    }
                },
                "originData": {
                    "name": "custom-action",
                    "width": 80,
                    "height": 50,
                    "label": "动作",
                    "parentKey": "custom"
                },
                "isCustom": true,
                "parentKey": "custom",
                "x": 671,
                "y": 612,
                "zIndex": 10,
                "incomingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5"
                        }
                    },
                    "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd:b622ae94-b250-40ac-b003-54ff27e104bd",
                    "targetPortId": "b622ae94-b250-40ac-b003-54ff27e104bd",
                    "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                    "zIndex": 1,
                    "data": {
                        "targetPortId": "b622ae94-b250-40ac-b003-54ff27e104bd",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                            "port": "b622ae94-b250-40ac-b003-54ff27e104bd"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "b622ae94-b250-40ac-b003-54ff27e104bd",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "target": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd:b622ae94-b250-40ac-b003-54ff27e104bd",
                        "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "targetPort": "b622ae94-b250-40ac-b003-54ff27e104bd"
                    },
                    "source": {
                        "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                        "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                    },
                    "target": {
                        "cell": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                        "port": "b622ae94-b250-40ac-b003-54ff27e104bd"
                    },
                    "labels": [{
                        "targetPortId": "b622ae94-b250-40ac-b003-54ff27e104bd",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                            "port": "b622ae94-b250-40ac-b003-54ff27e104bd"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "b622ae94-b250-40ac-b003-54ff27e104bd",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "target": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd:b622ae94-b250-40ac-b003-54ff27e104bd",
                        "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "targetPort": "b622ae94-b250-40ac-b003-54ff27e104bd"
                    }]
                }],
                "outgoingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5"
                        }
                    },
                    "id": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd:76af2d11-e7c5-44e8-9879-e417d1fd42eb-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "sourcePortId": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                    "zIndex": 1,
                    "data": {
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                        "source": {
                            "cell": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                            "port": "76af2d11-e7c5-44e8-9879-e417d1fd42eb"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                            "source": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                            "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd:76af2d11-e7c5-44e8-9879-e417d1fd42eb-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePort": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                        "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    },
                    "source": {
                        "cell": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                        "port": "76af2d11-e7c5-44e8-9879-e417d1fd42eb"
                    },
                    "target": {
                        "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                        "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    },
                    "labels": [{
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                        "source": {
                            "cell": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                            "port": "76af2d11-e7c5-44e8-9879-e417d1fd42eb"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                            "source": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                            "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd:76af2d11-e7c5-44e8-9879-e417d1fd42eb-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePort": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                        "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    }]
                }],
                "stroke": "#50e3c2",
                "fill": "#ffffff",
                "fontFill": "#50e3c2"
            },
            {
                "id": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                "renderKey": "custom-action",
                "name": "custom-action",
                "label": "航线部",
                "width": 80,
                "height": 50,
                "ports": {
                    "items": [{
                            "group": "top",
                            "id": "98662d47-3808-4e49-b3d7-e99f38a0b7d3"
                        },
                        {
                            "group": "right",
                            "id": "0785814a-4745-4181-9040-32719c606435"
                        },
                        {
                            "group": "bottom",
                            "id": "6fa9f560-7e0f-4a90-affc-a64ceee400f4"
                        },
                        {
                            "group": "left",
                            "id": "78a195b1-8141-422d-ba07-5c6bb0fd6192"
                        }
                    ],
                    "groups": {
                        "top": {
                            "position": {
                                "name": "top"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "right": {
                            "position": {
                                "name": "right"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "bottom": {
                            "position": {
                                "name": "bottom"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "left": {
                            "position": {
                                "name": "left"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        }
                    }
                },
                "originData": {
                    "name": "custom-action",
                    "width": 80,
                    "height": 50,
                    "label": "动作",
                    "parentKey": "custom"
                },
                "isCustom": true,
                "parentKey": "custom",
                "x": 268,
                "y": 612,
                "zIndex": 10,
                "incomingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5"
                        }
                    },
                    "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-1c17c04c-9283-42a4-aed4-fac6bdde195f:98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                    "targetPortId": "98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                    "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                    "zIndex": 1,
                    "data": {
                        "targetPortId": "98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                            "port": "98662d47-3808-4e49-b3d7-e99f38a0b7d3"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "target": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-1c17c04c-9283-42a4-aed4-fac6bdde195f:98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                        "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "targetPort": "98662d47-3808-4e49-b3d7-e99f38a0b7d3"
                    },
                    "source": {
                        "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                        "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                    },
                    "target": {
                        "cell": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                        "port": "98662d47-3808-4e49-b3d7-e99f38a0b7d3"
                    },
                    "labels": [{
                        "targetPortId": "98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                            "port": "98662d47-3808-4e49-b3d7-e99f38a0b7d3"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "target": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-1c17c04c-9283-42a4-aed4-fac6bdde195f:98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                        "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "targetPort": "98662d47-3808-4e49-b3d7-e99f38a0b7d3"
                    }]
                }],
                "outgoingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5"
                        }
                    },
                    "id": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f:6fa9f560-7e0f-4a90-affc-a64ceee400f4-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "sourcePortId": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                    "zIndex": 1,
                    "data": {
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                        "source": {
                            "cell": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                            "port": "6fa9f560-7e0f-4a90-affc-a64ceee400f4"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                            "source": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                            "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f:6fa9f560-7e0f-4a90-affc-a64ceee400f4-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePort": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                        "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    },
                    "source": {
                        "cell": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                        "port": "6fa9f560-7e0f-4a90-affc-a64ceee400f4"
                    },
                    "target": {
                        "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                        "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    },
                    "labels": [{
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                        "source": {
                            "cell": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                            "port": "6fa9f560-7e0f-4a90-affc-a64ceee400f4"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                            "source": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                            "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f:6fa9f560-7e0f-4a90-affc-a64ceee400f4-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePort": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                        "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    }]
                }],
                "fill": "#ffffff"
            },
            {
                "id": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                "renderKey": "custom-action",
                "name": "custom-action",
                "label": "操作部",
                "width": 80,
                "height": 50,
                "ports": {
                    "items": [{
                            "group": "top",
                            "id": "80141f60-f2f8-4e62-b228-bef6371003dc"
                        },
                        {
                            "group": "right",
                            "id": "dca26abb-64c7-47ef-bb1a-21d718e7514b"
                        },
                        {
                            "group": "bottom",
                            "id": "316468f3-7c63-439e-a740-4e3ac6bd3079"
                        },
                        {
                            "group": "left",
                            "id": "58c89b9b-5472-4105-9a1b-92795d12ae66"
                        }
                    ],
                    "groups": {
                        "top": {
                            "position": {
                                "name": "top"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "right": {
                            "position": {
                                "name": "right"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "bottom": {
                            "position": {
                                "name": "bottom"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "left": {
                            "position": {
                                "name": "left"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        }
                    }
                },
                "originData": {
                    "name": "custom-action",
                    "width": 80,
                    "height": 50,
                    "label": "动作",
                    "parentKey": "custom"
                },
                "isCustom": true,
                "parentKey": "custom",
                "x": 139,
                "y": 612,
                "zIndex": 10,
                "incomingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5"
                        }
                    },
                    "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-80ff002d-4759-4d66-9d0b-de645012d1e6:80141f60-f2f8-4e62-b228-bef6371003dc",
                    "targetPortId": "80141f60-f2f8-4e62-b228-bef6371003dc",
                    "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                    "zIndex": 1,
                    "data": {
                        "targetPortId": "80141f60-f2f8-4e62-b228-bef6371003dc",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                            "port": "80141f60-f2f8-4e62-b228-bef6371003dc"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "80141f60-f2f8-4e62-b228-bef6371003dc",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "target": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-80ff002d-4759-4d66-9d0b-de645012d1e6:80141f60-f2f8-4e62-b228-bef6371003dc",
                        "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "targetPort": "80141f60-f2f8-4e62-b228-bef6371003dc"
                    },
                    "source": {
                        "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                        "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                    },
                    "target": {
                        "cell": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                        "port": "80141f60-f2f8-4e62-b228-bef6371003dc"
                    },
                    "labels": [{
                        "targetPortId": "80141f60-f2f8-4e62-b228-bef6371003dc",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                            "port": "80141f60-f2f8-4e62-b228-bef6371003dc"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "80141f60-f2f8-4e62-b228-bef6371003dc",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "target": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-80ff002d-4759-4d66-9d0b-de645012d1e6:80141f60-f2f8-4e62-b228-bef6371003dc",
                        "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "targetPort": "80141f60-f2f8-4e62-b228-bef6371003dc"
                    }]
                }],
                "outgoingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5"
                        }
                    },
                    "id": "node-80ff002d-4759-4d66-9d0b-de645012d1e6:316468f3-7c63-439e-a740-4e3ac6bd3079-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "sourcePortId": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                    "zIndex": 1,
                    "data": {
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                        "source": {
                            "cell": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                            "port": "316468f3-7c63-439e-a740-4e3ac6bd3079"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                            "source": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                            "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-80ff002d-4759-4d66-9d0b-de645012d1e6:316468f3-7c63-439e-a740-4e3ac6bd3079-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePort": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                        "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    },
                    "source": {
                        "cell": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                        "port": "316468f3-7c63-439e-a740-4e3ac6bd3079"
                    },
                    "target": {
                        "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                        "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    },
                    "labels": [{
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                        "source": {
                            "cell": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                            "port": "316468f3-7c63-439e-a740-4e3ac6bd3079"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                            "source": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                            "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-80ff002d-4759-4d66-9d0b-de645012d1e6:316468f3-7c63-439e-a740-4e3ac6bd3079-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePort": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                        "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    }]
                }],
                "stroke": "#bd10e0",
                "fill": "#ffffff",
                "fontFill": "#9013fe"
            },
            {
                "id": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                "renderKey": "custom-action",
                "name": "custom-action",
                "label": "订舱部",
                "width": 80,
                "height": 50,
                "ports": {
                    "items": [{
                            "group": "top",
                            "id": "39dd9d6d-267f-4841-bde6-533b40ae5dd0"
                        },
                        {
                            "group": "right",
                            "id": "3b213f87-673b-43dd-9d1f-ebf475e39141"
                        },
                        {
                            "group": "bottom",
                            "id": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5"
                        },
                        {
                            "group": "left",
                            "id": "7338b0f0-79b2-47bd-8a93-531fe99381d7"
                        }
                    ],
                    "groups": {
                        "top": {
                            "position": {
                                "name": "top"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "right": {
                            "position": {
                                "name": "right"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "bottom": {
                            "position": {
                                "name": "bottom"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "left": {
                            "position": {
                                "name": "left"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        }
                    }
                },
                "originData": {
                    "name": "custom-action",
                    "width": 80,
                    "height": 50,
                    "label": "动作",
                    "parentKey": "custom"
                },
                "isCustom": true,
                "parentKey": "custom",
                "x": 811,
                "y": 612,
                "zIndex": 10,
                "incomingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5"
                        }
                    },
                    "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-f16c4260-b46e-4308-9d8b-428d1c9a42e0:39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                    "targetPortId": "39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                    "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                    "zIndex": 1,
                    "data": {
                        "targetPortId": "39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                            "port": "39dd9d6d-267f-4841-bde6-533b40ae5dd0"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "target": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-f16c4260-b46e-4308-9d8b-428d1c9a42e0:39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                        "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "targetPort": "39dd9d6d-267f-4841-bde6-533b40ae5dd0"
                    },
                    "source": {
                        "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                        "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                    },
                    "target": {
                        "cell": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                        "port": "39dd9d6d-267f-4841-bde6-533b40ae5dd0"
                    },
                    "labels": [{
                        "targetPortId": "39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                        "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "source": {
                            "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                        },
                        "target": {
                            "cell": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                            "port": "39dd9d6d-267f-4841-bde6-533b40ae5dd0"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                            "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                            "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                            "target": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-f16c4260-b46e-4308-9d8b-428d1c9a42e0:39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                        "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                        "targetPort": "39dd9d6d-267f-4841-bde6-533b40ae5dd0"
                    }]
                }],
                "outgoingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5"
                        }
                    },
                    "id": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0:6aa78a3c-c8f8-4e6c-bf5d-de671c845da5-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "sourcePortId": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                    "zIndex": 1,
                    "data": {
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                        "source": {
                            "cell": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                            "port": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                            "source": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                            "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0:6aa78a3c-c8f8-4e6c-bf5d-de671c845da5-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePort": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                        "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    },
                    "source": {
                        "cell": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                        "port": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5"
                    },
                    "target": {
                        "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                        "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    },
                    "labels": [{
                        "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePortId": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                        "source": {
                            "cell": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                            "port": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5"
                        },
                        "target": {
                            "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                            "sourcePortId": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                            "source": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                            "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0:6aa78a3c-c8f8-4e6c-bf5d-de671c845da5-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                        "sourcePort": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                        "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                    }]
                }],
                "stroke": "#d0021b",
                "fontFill": "#d0021b",
                "fill": "#ffffff"
            },
            {
                "id": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                "renderKey": "custom-action",
                "name": "custom-action",
                "label": "申请",
                "width": 60,
                "height": 60,
                "ports": {
                    "items": [{
                            "group": "top",
                            "id": "feb162f5-7313-4d55-a71a-fdcfa9dd914f"
                        },
                        {
                            "group": "right",
                            "id": "88c2e1df-2704-449b-ae94-a75aeafd8d8e"
                        },
                        {
                            "group": "bottom",
                            "id": "326fe1d8-257d-4caf-99d5-a7b30076a75f"
                        },
                        {
                            "group": "left",
                            "id": "5dfac3e0-9a36-4927-acef-1dcfb149b13e"
                        }
                    ],
                    "groups": {
                        "top": {
                            "position": {
                                "name": "top"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "right": {
                            "position": {
                                "name": "right"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "bottom": {
                            "position": {
                                "name": "bottom"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        },
                        "left": {
                            "position": {
                                "name": "left"
                            },
                            "attrs": {
                                "circle": {
                                    "r": 4,
                                    "magnet": true,
                                    "stroke": "#31d0c6",
                                    "strokeWidth": 2,
                                    "fill": "#fff",
                                    "style": {
                                        "visibility": "hidden"
                                    }
                                }
                            },
                            "zIndex": 10
                        }
                    }
                },
                "originData": {
                    "name": "custom-action",
                    "width": 60,
                    "height": 60,
                    "label": "动作",
                    "parentKey": "custom"
                },
                "isCustom": true,
                "parentKey": "custom",
                "x": 480,
                "y": 245,
                "zIndex": 10,
                "incomingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": [
                                0,
                                0
                            ]
                        }
                    },
                    "id": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12:4caac0e5-2256-49da-8215-cd64b56dd742-node-0a1971fa-5f9d-4895-8931-b81e0199ba1c:feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                    "targetPortId": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                    "sourcePortId": "4caac0e5-2256-49da-8215-cd64b56dd742",
                    "zIndex": 1,
                    "data": {
                        "id": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12:4caac0e5-2256-49da-8215-cd64b56dd742-node-0a1971fa-5f9d-4895-8931-b81e0199ba1c:feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                        "targetPortId": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                        "sourcePortId": "4caac0e5-2256-49da-8215-cd64b56dd742",
                        "source": {
                            "cell": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12",
                            "port": "4caac0e5-2256-49da-8215-cd64b56dd742"
                        },
                        "target": {
                            "cell": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                            "port": "feb162f5-7313-4d55-a71a-fdcfa9dd914f"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                            "sourcePortId": "4caac0e5-2256-49da-8215-cd64b56dd742",
                            "source": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12",
                            "target": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "4caac0e5-2256-49da-8215-cd64b56dd742",
                        "targetPort": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                        "strokeDasharray": [
                            0,
                            0
                        ]
                    },
                    "source": {
                        "cell": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12",
                        "port": "4caac0e5-2256-49da-8215-cd64b56dd742"
                    },
                    "target": {
                        "cell": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                        "port": "feb162f5-7313-4d55-a71a-fdcfa9dd914f"
                    },
                    "labels": [{
                        "targetPortId": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                        "sourcePortId": "4caac0e5-2256-49da-8215-cd64b56dd742",
                        "source": {
                            "cell": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12",
                            "port": "4caac0e5-2256-49da-8215-cd64b56dd742"
                        },
                        "target": {
                            "cell": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                            "port": "feb162f5-7313-4d55-a71a-fdcfa9dd914f"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": "5 5",
                                "strokeWidth": 1
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                            "sourcePortId": "4caac0e5-2256-49da-8215-cd64b56dd742",
                            "source": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12",
                            "target": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "id": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12:4caac0e5-2256-49da-8215-cd64b56dd742-node-0a1971fa-5f9d-4895-8931-b81e0199ba1c:feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                        "sourcePort": "4caac0e5-2256-49da-8215-cd64b56dd742",
                        "targetPort": "feb162f5-7313-4d55-a71a-fdcfa9dd914f"
                    }]
                }],
                "outgoingEdges": [{
                    "shape": "edge",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "strokeWidth": 1,
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": [
                                0,
                                0
                            ],
                            "label": "提交"
                        }
                    },
                    "id": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c:326fe1d8-257d-4caf-99d5-a7b30076a75f-node-4a9b66a4-334d-4870-9705-5e91d4c95486:3d998928-2420-44df-a015-f6087c6adf45",
                    "targetPortId": "3d998928-2420-44df-a015-f6087c6adf45",
                    "sourcePortId": "326fe1d8-257d-4caf-99d5-a7b30076a75f",
                    "zIndex": 1,
                    "data": {
                        "id": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c:326fe1d8-257d-4caf-99d5-a7b30076a75f-node-4a9b66a4-334d-4870-9705-5e91d4c95486:3d998928-2420-44df-a015-f6087c6adf45",
                        "targetPortId": "3d998928-2420-44df-a015-f6087c6adf45",
                        "sourcePortId": "326fe1d8-257d-4caf-99d5-a7b30076a75f",
                        "source": {
                            "cell": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                            "port": "326fe1d8-257d-4caf-99d5-a7b30076a75f"
                        },
                        "target": {
                            "cell": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                            "port": "3d998928-2420-44df-a015-f6087c6adf45"
                        },
                        "attrs": {
                            "line": {
                                "stroke": "#A2B1C3",
                                "targetMarker": {
                                    "name": "block",
                                    "width": 12,
                                    "height": 8
                                },
                                "strokeDasharray": [
                                    0,
                                    0
                                ],
                                "strokeWidth": 1,
                                "label": "提交"
                            }
                        },
                        "zIndex": 1,
                        "data": {
                            "targetPortId": "3d998928-2420-44df-a015-f6087c6adf45",
                            "sourcePortId": "326fe1d8-257d-4caf-99d5-a7b30076a75f",
                            "source": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                            "target": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                            "attrs": {
                                "line": {
                                    "stroke": "#A2B1C3",
                                    "targetMarker": {
                                        "name": "block",
                                        "width": 12,
                                        "height": 8
                                    },
                                    "strokeDasharray": "5 5",
                                    "strokeWidth": 1
                                }
                            }
                        },
                        "sourcePort": "326fe1d8-257d-4caf-99d5-a7b30076a75f",
                        "targetPort": "3d998928-2420-44df-a015-f6087c6adf45",
                        "strokeDasharray": [
                            0,
                            0
                        ],
                        "label": "提交"
                    },
                    "source": {
                        "cell": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                        "port": "326fe1d8-257d-4caf-99d5-a7b30076a75f"
                    },
                    "target": {
                        "cell": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                        "port": "3d998928-2420-44df-a015-f6087c6adf45"
                    },
                    "labels": [{
                        "attrs": {
                            "label": {
                                "text": "提交"
                            }
                        }
                    }]
                }]
            }
        ],
        "edges": [{
                "id": "node-4a9b66a4-334d-4870-9705-5e91d4c95486:afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9-node-90845bad-2898-402b-afec-3811eafa5689:54ef7109-d40b-4094-8f23-c415ef483617",
                "targetPortId": "54ef7109-d40b-4094-8f23-c415ef483617",
                "sourcePortId": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                "source": {
                    "cell": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                    "port": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9"
                },
                "target": {
                    "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                    "port": "54ef7109-d40b-4094-8f23-c415ef483617"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": [
                            0,
                            0
                        ],
                        "strokeWidth": 1
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "54ef7109-d40b-4094-8f23-c415ef483617",
                    "sourcePortId": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                    "source": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                    "target": "node-90845bad-2898-402b-afec-3811eafa5689",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "afd65c88-c3ea-42d1-8684-1cf4cb7e3ff9",
                "targetPort": "54ef7109-d40b-4094-8f23-c415ef483617",
                "strokeDasharray": [
                    0,
                    0
                ]
            },
            {
                "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-c133a553-2165-4917-9ce6-b201adccab43:37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                "targetPortId": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                "source": {
                    "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                    "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                },
                "target": {
                    "cell": "node-c133a553-2165-4917-9ce6-b201adccab43",
                    "port": "37c3f28d-30eb-4f8d-954b-cd9026e3650a"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": [
                            0,
                            0
                        ],
                        "strokeWidth": 1
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                    "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                    "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                    "target": "node-c133a553-2165-4917-9ce6-b201adccab43",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                "targetPort": "37c3f28d-30eb-4f8d-954b-cd9026e3650a",
                "strokeDasharray": [
                    0,
                    0
                ]
            },
            {
                "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3:85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                "targetPortId": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                "source": {
                    "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                    "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                },
                "target": {
                    "cell": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                    "port": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": [
                            0,
                            0
                        ],
                        "strokeWidth": 1
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                    "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                    "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                    "target": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                "targetPort": "85eeca5d-3d41-43b0-ab2f-837eaf88fa1d",
                "strokeDasharray": [
                    0,
                    0
                ]
            },
            {
                "id": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3:a5ff8093-70e2-448f-a0fe-3d69060fd457-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                "sourcePortId": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                "source": {
                    "cell": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                    "port": "a5ff8093-70e2-448f-a0fe-3d69060fd457"
                },
                "target": {
                    "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                    "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": [
                            0,
                            0
                        ],
                        "strokeWidth": 1
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "sourcePortId": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                    "source": "node-35347ba9-f2dd-4c27-8d9a-133d0f2f20b3",
                    "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "a5ff8093-70e2-448f-a0fe-3d69060fd457",
                "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                "strokeDasharray": [
                    0,
                    0
                ]
            },
            {
                "id": "node-c133a553-2165-4917-9ce6-b201adccab43:06071849-6e12-4060-97cc-b214d9f2290a-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                "sourcePortId": "06071849-6e12-4060-97cc-b214d9f2290a",
                "source": {
                    "cell": "node-c133a553-2165-4917-9ce6-b201adccab43",
                    "port": "06071849-6e12-4060-97cc-b214d9f2290a"
                },
                "target": {
                    "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                    "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": [
                            0,
                            0
                        ],
                        "strokeWidth": 1
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "sourcePortId": "06071849-6e12-4060-97cc-b214d9f2290a",
                    "source": "node-c133a553-2165-4917-9ce6-b201adccab43",
                    "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "06071849-6e12-4060-97cc-b214d9f2290a",
                "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                "strokeDasharray": [
                    0,
                    0
                ]
            },
            {
                "id": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:15399998-fa33-4df2-b3b8-0cdac120b888-node-97aba55f-352f-45b4-bf5c-8b195e2e3f74:634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                "targetPortId": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                "sourcePortId": "15399998-fa33-4df2-b3b8-0cdac120b888",
                "source": {
                    "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                    "port": "15399998-fa33-4df2-b3b8-0cdac120b888"
                },
                "target": {
                    "cell": "node-97aba55f-352f-45b4-bf5c-8b195e2e3f74",
                    "port": "634b2ee9-537f-4cca-93e9-e8caeb3235aa"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": [
                            0,
                            0
                        ],
                        "strokeWidth": 1
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                    "sourcePortId": "15399998-fa33-4df2-b3b8-0cdac120b888",
                    "source": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                    "target": "node-97aba55f-352f-45b4-bf5c-8b195e2e3f74",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "15399998-fa33-4df2-b3b8-0cdac120b888",
                "targetPort": "634b2ee9-537f-4cca-93e9-e8caeb3235aa",
                "strokeDasharray": [
                    0,
                    0
                ]
            },
            {
                "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-80ff002d-4759-4d66-9d0b-de645012d1e6:80141f60-f2f8-4e62-b228-bef6371003dc",
                "targetPortId": "80141f60-f2f8-4e62-b228-bef6371003dc",
                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                "source": {
                    "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                    "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                },
                "target": {
                    "cell": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                    "port": "80141f60-f2f8-4e62-b228-bef6371003dc"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": "5 5",
                        "strokeWidth": 1
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "80141f60-f2f8-4e62-b228-bef6371003dc",
                    "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                    "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                    "target": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                "targetPort": "80141f60-f2f8-4e62-b228-bef6371003dc"
            },
            {
                "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-1c17c04c-9283-42a4-aed4-fac6bdde195f:98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                "targetPortId": "98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                "source": {
                    "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                    "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                },
                "target": {
                    "cell": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                    "port": "98662d47-3808-4e49-b3d7-e99f38a0b7d3"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": "5 5",
                        "strokeWidth": 1
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "98662d47-3808-4e49-b3d7-e99f38a0b7d3",
                    "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                    "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                    "target": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                "targetPort": "98662d47-3808-4e49-b3d7-e99f38a0b7d3"
            },
            {
                "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd:b622ae94-b250-40ac-b003-54ff27e104bd",
                "targetPortId": "b622ae94-b250-40ac-b003-54ff27e104bd",
                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                "source": {
                    "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                    "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                },
                "target": {
                    "cell": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                    "port": "b622ae94-b250-40ac-b003-54ff27e104bd"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": "5 5",
                        "strokeWidth": 1
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "b622ae94-b250-40ac-b003-54ff27e104bd",
                    "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                    "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                    "target": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                "targetPort": "b622ae94-b250-40ac-b003-54ff27e104bd"
            },
            {
                "id": "node-80ff002d-4759-4d66-9d0b-de645012d1e6:316468f3-7c63-439e-a740-4e3ac6bd3079-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                "sourcePortId": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                "source": {
                    "cell": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                    "port": "316468f3-7c63-439e-a740-4e3ac6bd3079"
                },
                "target": {
                    "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                    "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": "5 5",
                        "strokeWidth": 1
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "sourcePortId": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                    "source": "node-80ff002d-4759-4d66-9d0b-de645012d1e6",
                    "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "316468f3-7c63-439e-a740-4e3ac6bd3079",
                "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
            },
            {
                "id": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f:6fa9f560-7e0f-4a90-affc-a64ceee400f4-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                "sourcePortId": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                "source": {
                    "cell": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                    "port": "6fa9f560-7e0f-4a90-affc-a64ceee400f4"
                },
                "target": {
                    "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                    "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": "5 5",
                        "strokeWidth": 1
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "sourcePortId": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                    "source": "node-1c17c04c-9283-42a4-aed4-fac6bdde195f",
                    "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "6fa9f560-7e0f-4a90-affc-a64ceee400f4",
                "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
            },
            {
                "id": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd:76af2d11-e7c5-44e8-9879-e417d1fd42eb-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                "sourcePortId": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                "source": {
                    "cell": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                    "port": "76af2d11-e7c5-44e8-9879-e417d1fd42eb"
                },
                "target": {
                    "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                    "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": "5 5",
                        "strokeWidth": 1
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "sourcePortId": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                    "source": "node-a1a6c55b-b29b-4a5a-bf64-03ca1d8e61cd",
                    "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "76af2d11-e7c5-44e8-9879-e417d1fd42eb",
                "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
            },
            {
                "id": "node-90845bad-2898-402b-afec-3811eafa5689:20f07b04-269b-4ab0-9922-1ba6c9ce61d1-node-f16c4260-b46e-4308-9d8b-428d1c9a42e0:39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                "targetPortId": "39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                "source": {
                    "cell": "node-90845bad-2898-402b-afec-3811eafa5689",
                    "port": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1"
                },
                "target": {
                    "cell": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                    "port": "39dd9d6d-267f-4841-bde6-533b40ae5dd0"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": "5 5",
                        "strokeWidth": 1
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "39dd9d6d-267f-4841-bde6-533b40ae5dd0",
                    "sourcePortId": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                    "source": "node-90845bad-2898-402b-afec-3811eafa5689",
                    "target": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "20f07b04-269b-4ab0-9922-1ba6c9ce61d1",
                "targetPort": "39dd9d6d-267f-4841-bde6-533b40ae5dd0"
            },
            {
                "id": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0:6aa78a3c-c8f8-4e6c-bf5d-de671c845da5-node-de175d19-22d3-4dfe-ba1a-8d8a34142b62:f6ca1227-268a-488e-acb2-a4f4362fadaa",
                "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                "sourcePortId": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                "source": {
                    "cell": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                    "port": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5"
                },
                "target": {
                    "cell": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                    "port": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": "5 5",
                        "strokeWidth": 1
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "f6ca1227-268a-488e-acb2-a4f4362fadaa",
                    "sourcePortId": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                    "source": "node-f16c4260-b46e-4308-9d8b-428d1c9a42e0",
                    "target": "node-de175d19-22d3-4dfe-ba1a-8d8a34142b62",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "6aa78a3c-c8f8-4e6c-bf5d-de671c845da5",
                "targetPort": "f6ca1227-268a-488e-acb2-a4f4362fadaa"
            },
            {
                "id": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12:4caac0e5-2256-49da-8215-cd64b56dd742-node-0a1971fa-5f9d-4895-8931-b81e0199ba1c:feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                "targetPortId": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                "sourcePortId": "4caac0e5-2256-49da-8215-cd64b56dd742",
                "source": {
                    "cell": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12",
                    "port": "4caac0e5-2256-49da-8215-cd64b56dd742"
                },
                "target": {
                    "cell": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                    "port": "feb162f5-7313-4d55-a71a-fdcfa9dd914f"
                },
                "attrs": {
                    "line": {
                        "stroke": "#508F08",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": [
                            0,
                            0
                        ],
                        "strokeWidth": 1,
                        "strokeDasharray": 4,
                        "style": {
                            "animation": "ant-line 30s infinite linear",
                          },
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                    "sourcePortId": "4caac0e5-2256-49da-8215-cd64b56dd742",
                    "source": "node-263d0acf-74f5-48d8-bb47-02938e7f0c12",
                    "target": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "4caac0e5-2256-49da-8215-cd64b56dd742",
                "targetPort": "feb162f5-7313-4d55-a71a-fdcfa9dd914f",
                "strokeDasharray": [
                    0,
                    0
                ]
            },
            {
                "id": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c:326fe1d8-257d-4caf-99d5-a7b30076a75f-node-4a9b66a4-334d-4870-9705-5e91d4c95486:3d998928-2420-44df-a015-f6087c6adf45",
                "targetPortId": "3d998928-2420-44df-a015-f6087c6adf45",
                "sourcePortId": "326fe1d8-257d-4caf-99d5-a7b30076a75f",
                "source": {
                    "cell": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                    "port": "326fe1d8-257d-4caf-99d5-a7b30076a75f"
                },
                "target": {
                    "cell": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                    "port": "3d998928-2420-44df-a015-f6087c6adf45"
                },
                "attrs": {
                    "line": {
                        "stroke": "#A2B1C3",
                        "targetMarker": {
                            "name": "block",
                            "width": 12,
                            "height": 8
                        },
                        "strokeDasharray": [
                            0,
                            0
                        ],
                        "strokeWidth": 1,
                        "label": "提交"
                    }
                },
                "zIndex": 1,
                "data": {
                    "targetPortId": "3d998928-2420-44df-a015-f6087c6adf45",
                    "sourcePortId": "326fe1d8-257d-4caf-99d5-a7b30076a75f",
                    "source": "node-0a1971fa-5f9d-4895-8931-b81e0199ba1c",
                    "target": "node-4a9b66a4-334d-4870-9705-5e91d4c95486",
                    "attrs": {
                        "line": {
                            "stroke": "#A2B1C3",
                            "targetMarker": {
                                "name": "block",
                                "width": 12,
                                "height": 8
                            },
                            "strokeDasharray": "5 5",
                            "strokeWidth": 1
                        }
                    }
                },
                "sourcePort": "326fe1d8-257d-4caf-99d5-a7b30076a75f",
                "targetPort": "3d998928-2420-44df-a015-f6087c6adf45",
                "strokeDasharray": [
                    0,
                    0
                ],
                "label": "提交"
            }
        ]
    };


    return data
}