import React, { useState, useEffect } from 'react';
import { Card, Row, Col, Button, Modal, Space, message } from 'antd';
import { VerticalRightOutlined, LeftOutlined, RightOutlined, VerticalLeftOutlined } from '@ant-design/icons';
import { Worker, Viewer } from '@react-pdf-viewer/core';
import { pageNavigationPlugin } from '@react-pdf-viewer/page-navigation';

import '@react-pdf-viewer/core/lib/styles/index.css';

export default () => {
  const [modelShow, setModelShow] = useState(false);

  const [pageTotal, setPageTotal] = useState(null);
  const [pageNumber, setPageNumber] = useState(0);

  const pageNavigationPluginInstance = pageNavigationPlugin();
  const { GoToFirstPage, GoToLastPage, GoToNextPage, GoToPreviousPage } = pageNavigationPluginInstance;

  const [url, setUrl] = useState('');

  const onChange = (e) => {
    const files = e.target.files;
    files.length > 0 && setUrl(window.URL.createObjectURL(files[0]));
  };

  const pageLayout = {
    transformSize: ({ numPages, pageIndex, size }) => {
      setPageTotal(numPages);

      return ({
        height: size.height + 30,
        width: size.width + 30,
      })
    },
  };

  return (<Card >
    <Row>
      <Col span={8}>
        <input type="file" accept=".pdf" onChange={onChange} />
      </Col>
      <Col span={4}>
        <Button type="primary" onClick={() => setModelShow(true)}>
          新建签章
        </Button>
      </Col>
    </Row>

    <Modal title="电子签章" maskClosable={false} centered width={800} bodyStyle={{ height: "calc(100vh - 200px)", overflow: 'auto', padding: '0' }} open={modelShow} onCancel={() => setModelShow(false)}>

      {url ?
        <Worker workerUrl="https://unpkg.com/pdfjs-dist@3.2.146/build/pdf.worker.min.js">
          <Viewer
            defaultScale={0.9}
            fileUrl={url}
            onPageChange={(e) => { setPageNumber(e.currentPage); }}
            pageLayout={pageLayout}
            // initialPage={pageNumber}
            plugins={[pageNavigationPluginInstance]}
          />
          <Row style={{ width: '600px', position: 'absolute', bottom: '24px', left: '20px' }}>
            <Col span={16}>
              <Space>
                <GoToFirstPage>
                  {(props) => (
                    <Button
                      type="primary"
                      icon={<VerticalRightOutlined />}
                      size="small"
                      disabled={props.isDisabled}
                      onClick={props.onClick}
                    >
                      首页
                    </Button>
                  )}
                </GoToFirstPage>
                <GoToPreviousPage>
                  {(props) => (
                    <Button
                      type="primary"
                      icon={<LeftOutlined />}
                      size="small"
                      disabled={props.isDisabled}
                      onClick={props.onClick}
                    >
                      上页
                    </Button>
                  )}
                </GoToPreviousPage>
                <Button size="small" style={{ width: '70px' }}>
                  {pageNumber + 1}/{pageTotal}
                </Button>
                <GoToNextPage>
                  {(props) => (
                    <Button
                      type="primary"
                      icon={<RightOutlined />}
                      size="small"
                      disabled={props.isDisabled}
                      onClick={props.onClick}
                    >
                      下页
                    </Button>
                  )}
                </GoToNextPage>
                <GoToLastPage>
                  {(props) => (
                    <Button type="primary" icon={<VerticalLeftOutlined />} size="small"
                      disabled={props.isDisabled}
                      onClick={props.onClick}
                    >
                      尾页
                    </Button>
                  )}
                </GoToLastPage>
              </Space>
            </Col>
            <Col span={4}>

            </Col>
          </Row>
        </Worker>
        :
        <>请上传PDF文件</>
      }

    </Modal>
  </Card >);
}
