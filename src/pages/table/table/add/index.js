
import React, { useState, useEffect } from 'react';
import { Card, Form, Modal, Input, Button, AutoComplete,DatePicker } from 'antd';
import { useLocation } from 'umi';

export default (props) => {
  const [visible, setVisible] = useState(false);
  const [form] = Form.useForm();
  const location = useLocation();

  // console.log(location.query);// 参数接收
  form.setFieldsValue({userName:props?.match?.params?.userId=='add'?'':props?.match?.params?.userId})
  form.setFieldsValue({passWord:location.query?.test})

  const onFinish = (e) => {
    console.log(e);
  };

  const changeValue = (e) => {
    console.log(e.target.value);
  }

  const topValue = [
    { value: 'text 1', },
    { value: 'text 2', },
    { value: 'text 3', },
  ]

  // 转大写
  const handleChange = (event) => {
    // console.log(event.target.value)
    // setHelloTo(event.target.value.toUpperCase());
    form.setFieldsValue({ passWord: event.target.value.toUpperCase() })
  }

  const handleCancel = () => {
    setVisible(false);
  };


  return (<Card >
    <Form
      name="wrap"
      labelCol={{ flex: '100px' }}
      labelAlign="left"
      labelWrap
      wrapperCol={{ flex: 1 }}
      onFinish={onFinish}
      form={form}
    >
      <Form.Item label="正常标签" name="userName" rules={[{ required: true }]}>
        <Input />
      </Form.Item>
      <Form.Item label="时间" name="dateTime">
        <DatePicker />
      </Form.Item>
      <Form.Item label="自动大写" name="passWord" onBlur={handleChange}>
        <Input />
      </Form.Item>

      <Form.Item label="自动提示" name="topMessage">
        <AutoComplete
          style={{
            width: '100%',
          }}
          placeholder="置顶提示"
          options={topValue}
          onBlur={changeValue}
        >
        </AutoComplete>
      </Form.Item>

      <Form.Item>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>

        <Button danger onClick={() => { window.close() }} style={{marginLeft:'20px'}}>
          关闭
        </Button>

        <Button onClick={() => { setVisible(!visible) }} style={{marginLeft:'20px'}}>
          提示
        </Button>
      </Form.Item>
    </Form>

    <Modal title="Basic Modal" visible={visible} onCancel={handleCancel}>
      <p>Some contents...</p>
      <p>Some contents...</p>
      <p>Some contents...</p>
    </Modal>
  </Card>);
}
