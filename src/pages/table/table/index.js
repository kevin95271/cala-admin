import { useState } from 'react';
import { Form, Input, Button, AutoComplete, DatePicker } from 'antd';
import ProTable from '@/components/ResizableProTable';
import { history } from 'umi';
import { NewWindow } from '@/utils/NewWindow';
import DragModal from '@/components/DragModal';

export default (props) => {
  const [modalOpen, setmodalOpen] = useState(false);

  const columns = [
    {
      title: '序号',
      dataIndex: 'index',
      width: 60,
      align: 'center',
      valueType: 'indexBorder',
    },
    {
      title: '姓名',
      width: 200,
      dataIndex: 'name',
      copyable: true,
      ellipsis: true,
    },
    {
      title: '金额',
      dataIndex: 'title',
      width: 100,
      valueType: 'money',
      renderText: () => (Math.random() * 100).toFixed(2),
    },
    {
      title: '创建者',
      width: 100,
      dataIndex: 'creator',
    },
    {
      title: '创建时间',
      width: 180,
      align: 'center',
      dataIndex: 'createdAt',
      valueType: 'dateTime',
      hideInSearch: true,
    },
    {
      title: '创建时间', // 查询条件
      dataIndex: 'createdAt',
      valueType: 'dateRange',
      hideInTable: true,
      search: {
        transform: (value) => {
          return {
            startTime: value[0],
            endTime: value[1],
          };
        },
      },
    },
    {
      title: '备注',
      dataIndex: 'remark',
      hideInSearch: true,
    },
    {
      title: '操作栏',
      width: 100,
      align: 'center',
      search: false,
      render: (_, record) => (
        <Button key={record.name} size="small" onClick={() => { NewWindow('/new/table/add/' + record.name + '/提单详情') }}>
          编辑信息
        </Button>
      ),
    },
  ];

  const tableListDataSource = [];

  for (let i = 0; i < 255; i += 1) {
    tableListDataSource.push({
      key: i,
      name: 'AppName' + i,
      containers: Math.floor(Math.random() * 20),
      createdAt: Date.now() - Math.floor(Math.random() * 2000),
      creator: 'Sunny',
    });
  }

  const dataList = () => {
    return {
      data: tableListDataSource,
      success: true,
    };
  }
  const [form] = Form.useForm();
  const onFinish = (e) => {
    console.log(e);
  };

  const { offsetHeight } = window.document.getElementsByClassName("cala-body")[0]; //获取容器高度

  return (<>
    <ProTable
      rowKey="key"
      defaultSize="small"
      bordered
      scroll={{ x: 1600, y: offsetHeight - 322 }}
      columns={columns}
      request={dataList}
      rowSelection={{}}
      search={{ labelWidth: 'auto', span: 4 }}
      dateFormatter="string"
      headerTitle={<span>普通表格</span>}
      toolBarRender={() => [
        <Button key="1" type="primary" onClick={() => { setmodalOpen(true); }}>
          新建弹窗
        </Button>,
        <Button key="2" type="primary"
          onClick={() => { history.push('/table/table/add?test=abce') }}>
          新建标签
        </Button>,
        <Button key="3" type="primary" onClick={() => { NewWindow('/new/table/add/90807060/新建提单') }}>
          新建页面
        </Button>
      ]}
      pagination={{ size: 'small' }}
    />

    <DragModal
      title="新建表单"
      open={modalOpen}
      width={800}
      bodyStyle={{ height: '400px' }}
      onCancel={() => setmodalOpen(false)}
    >
      <Form
        name="wrap"
        labelCol={{ flex: '100px' }}
        labelAlign="left"
        labelWrap
        wrapperCol={{ flex: 1 }}
        onFinish={onFinish}
        form={form}
      >
        <Form.Item label="正常标签" name="userName" rules={[{ required: true }]}>
          <Input />
        </Form.Item>
        <Form.Item label="时间" name="dateTime">
          <DatePicker />
        </Form.Item>
        <Form.Item label="自动大写" name="passWord">
          <Input />
        </Form.Item>

        <Form.Item label="自动提示" name="topMessage">
          <AutoComplete
            style={{
              width: '100%',
            }}
            placeholder="置顶提示"
          >
          </AutoComplete>
        </Form.Item>
      </Form>
    </DragModal>
  </>);
}
